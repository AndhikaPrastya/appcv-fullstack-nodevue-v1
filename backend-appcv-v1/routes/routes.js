'use strict';

const express = require ('express');
// const router = express.Router();
const model = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const passport = require('passport');
const passportJWT = require('passport-jwt');


module.exports = function(app) {

// ekstrak jwt agar tamoil di rest api
let ExtractJwt = passportJWT.ExtractJwt;
let JwtStrategy = passportJWT.Strategy;

// object untuk menampung jwt
let jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = "gabutbener";

let strategy = new JwtStrategy(jwtOptions, (jwt_payload, next) => {
    let Person = getPerson({idPerson: jwt_payload.idPerson})
        if(Person) {
            next(null, Person);
        }else {
            next(null, false);
        }
});

passport.use(strategy);

// mengambil Person untuk bisa membuka routes yang di lovk
const getPerson = async obj => {
    return await model.Person.findOne({
        where: obj
    });
};

// login listing.
app.post('/api/login', async function(req, res, next) {
    try {
        const {
            email,
            password
        } = req.body;

        if (email && password) {
            let Person = await getPerson({email: email});

            if(!Person) {
                res.status(401).json({
                'status': 'ERROR',
                'messages': 'Incorrect email or password',
                'data': {},
                })
            }

            const match = await bcrypt.compare(password, Person.password);
            
            if(match) {
                let payload = {idPerson: Person.idPerson}

                let token = jwt.sign(payload, jwtOptions.secretOrKey)

                res.status(201).json({
                    'status': 'OK',
                    'token': token,
                    'values': Person,
                })

            }else {
                res.status(400).json({
                    'status': 'ERROR',
                    'messages': 'Incorrect password',
                })
            }
        }
    } catch (err) {
    res.status(400).json({
        'status': 'ERROR',
        'messages': err.message,
        'data': {},
    })
    }
});


    const registerRoutes = require('./../controllers/register-routes');
    app.use('/api/register', registerRoutes);

    const personRoutes = require('./../controllers/person-routes');
    app.use('/api/person', passport.authenticate('jwt', {session: false}), personRoutes);

    const languageSkillRoutes = require('./../controllers/languageSkill-routes');
    app.use('/api/language-skill', passport.authenticate('jwt', {session: false}), languageSkillRoutes);

    const objectiveRoutes = require('./../controllers/objective-routes');
    app.use('/api/objective', passport.authenticate('jwt', {session: false}), objectiveRoutes);

    const profileRoutes = require('./../controllers/profile-routes');
    app.use('/api/profile', passport.authenticate('jwt', {session: false}), profileRoutes);

    const educationRoutes = require('./../controllers/education-routes');
    app.use('/api/education', passport.authenticate('jwt', {session: false}), educationRoutes);

    const courseRoutes = require('./../controllers/course-routes');
    app.use('/api/course', passport.authenticate('jwt', {session: false}), courseRoutes);

    const companyRoutes = require('./../controllers/company-routes');
    app.use('/api/company', passport.authenticate('jwt', {session: false}), companyRoutes);

    const projectRoutes = require('./../controllers/project-routes');
    app.use('/api/project', passport.authenticate('jwt', {session: false}), projectRoutes);

    const roleRoutes = require('./../controllers/role-routes');
    app.use('/api/role', passport.authenticate('jwt', {session: false}), roleRoutes);

    const companyRoleRoutes = require('./../controllers/companyrole-routes');
    app.use('/api/company-role', passport.authenticate('jwt', {session: false}), companyRoleRoutes);
    
    const projectRoleRoutes = require('./../controllers/projectRole-routes');
    app.use('/api/project-role',  passport.authenticate('jwt', {session: false}), projectRoleRoutes);

    const projectPeriodeRoutes = require('./../controllers/projectperiode-routes');
    app.use('/api/project-periode', passport.authenticate('jwt', {session: false}), projectPeriodeRoutes);

    const generateDocxRoutes = require('./../controllers/generatedocx-routes');
    app.use('/api/generatedocx', passport.authenticate('jwt', {session: false}), generateDocxRoutes);
    

};