module.exports = (sequelize, DataTypes) => {
    const LanguageSkill = sequelize.define("LanguageSkill", {
        idLanguageSkill: {
            type: DataTypes.STRING(255),
            primaryKey: true,
            allowNull: false
        },
        language: {
            type: DataTypes.STRING(25),
            allowNull: false
        },
        level: {
            type: DataTypes.STRING(25),
            allowNull: false
        }

    });

    LanguageSkill.associate = models => {
        LanguageSkill.belongsTo(models.Person,{
            foreignKey: 'idPerson',
            allowNull: false
        });
    };

    return LanguageSkill;
};