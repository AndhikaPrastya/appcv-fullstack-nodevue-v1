module.exports = (sequelize, DataTypes) => {
    const Person = sequelize.define("Person", {
        idPerson: {
            type: DataTypes.STRING(255),
            primaryKey: true,
            allowNull: false
        },
        namePerson: {
            type: DataTypes.STRING(25),
            allowNull: false
        },
        birthPlace: {
            type: DataTypes.STRING(25)
        },
        birthDate: {
            type: DataTypes.DATEONLY
        },
        religion: {
            type: DataTypes.STRING(25),
        },
        gendre: {
            type: DataTypes.STRING(25)
        },
        health: {
            type: DataTypes.STRING(25)
        },
        email: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        password: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        photoPerson: {
            type: DataTypes.STRING(255),
        }

    });

    Person.associate = models => {

        Person.hasMany(models.LanguageSkill,{
            foreignKey:{
            name: 'idPerson',
            allowNull: false,
            onDelete: 'cascade',
            onUpdate: 'cascade'
            } 
        });

        Person.hasMany(models.Objective,{
            foreignKey:{
            name: 'idPerson',
            allowNull: false,
            onDelete: 'cascade',
            onUpdate: 'cascade'
            } 
        });

        Person.hasMany(models.Profile,{
            foreignKey:{
            name: 'idPerson',
            allowNull: false,
            onDelete: 'cascade',
            onUpdate: 'cascade'
            } 
        });

        Person.hasMany(models.Education,{
            foreignKey:{
            name: 'idPerson',
            allowNull: false,
            onDelete: 'cascade',
            onUpdate: 'cascade'
            } 
        });

        Person.hasMany(models.Course,{
            foreignKey:{
            name: 'idPerson',
            allowNull: false,
            onDelete: 'cascade',
            onUpdate: 'cascade'
            }
        });

        Person.hasMany(models.Company,{
            foreignKey:{
            name: 'idPerson',
            allowNull: false,
            onDelete: 'cascade',
            onUpdate: 'cascade'
            }
        });

        Person.hasMany(models.Project,{
            foreignKey:{
            name: 'idPerson',
            allowNull: false,
            onDelete: 'cascade',
            onUpdate: 'cascade'
            }
        });

        Person.hasMany(models.Role,{
            foreignKey:{
            name: 'idPerson',
            allowNull: false,
            onDelete: 'cascade',
            onUpdate: 'cascade'
            }
        });

        // Person.hasMany(models.ProjectPeriode,{
        //     foreignKey:{
        //     name: 'idPerson',
        //     allowNull: false,
        //     onDelete: 'cascade',
        //     onUpdate: 'cascade'
        //     }
        // });
    };

    return Person;
};