module.exports = (sequelize, DataTypes) => {
    const Project = sequelize.define("Project", {
        idProject: {
            type: DataTypes.STRING(255),
            primaryKey: true,
            allowNull: false
        },
        nameProject: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        customer: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        // priodFrom: {
        //     type: DataTypes.DATEONLY,
        //     allowNull: false
        // },
        // priodTo: {
        //     type: DataTypes.DATEONLY
        // },
        descProject: {
            type: DataTypes.STRING(200)
        },
        technicalInfo: {
            type: DataTypes.STRING(200)
        },
        appType: {
            type: DataTypes.STRING(50)
        },
        serverOs: {
            type: DataTypes.STRING(50)
        },
        database: {
            type: DataTypes.STRING(50)
        },
        appServer: {
            type: DataTypes.STRING(50)
        },
        framework: {
            type: DataTypes.STRING(50)
        },
        devTool: {
            type: DataTypes.STRING(50)
        },
        devLanguage: {
            type: DataTypes.STRING(50)
        },
        otherInfo: {
            type: DataTypes.STRING(200)
        }
    });

    Project.associate = models => {

        Project.belongsTo(models.Person,{
            foreignKey: 'idPerson',
            allowNull: false
        });

        Project.hasMany(models.ProjectRole,{
            foreignKey:{
            name: 'idProject',
            allowNull: false,
            onDelete: 'cascade',
            onUpdate: 'cascade'
            } 
        });

        Project.hasMany(models.ProjectPeriode,{
            foreignKey:{
            name: 'idProject',
            allowNull: false,
            onDelete: 'cascade',
            onUpdate: 'cascade'
            } 
        });
        
    };

    return Project;
};