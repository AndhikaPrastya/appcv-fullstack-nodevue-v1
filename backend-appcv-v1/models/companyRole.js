module.exports = (sequelize, DataTypes) => {
    const CompanyRole = sequelize.define("CompanyRole", {
        idCompanyRole: {
            type: DataTypes.STRING(255),
            primaryKey: true,
            allowNull: false
        },
    });

    CompanyRole.associate = models => {
        CompanyRole.belongsTo(models.Company,{
            foreignKey: 'idCompany',
            allowNull: false
        });
        CompanyRole.belongsTo(models.Role,{
            foreignKey: 'idRole',
            allowNull: false
        });
    };


    return CompanyRole;
};