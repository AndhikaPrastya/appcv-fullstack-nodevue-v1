module.exports = (sequelize, DataTypes) => {
    const ProjectPeriode = sequelize.define("ProjectPeriode", {
        idProjectPeriode: {
            type: DataTypes.STRING(255),
            primaryKey: true,
            allowNull: false
        },
        priodFrom: {
            type: DataTypes.DATEONLY,
            allowNull: false
        },
        priodTo: {
            type: DataTypes.DATEONLY
        },
    });

    ProjectPeriode.associate = models => {
        ProjectPeriode.belongsTo(models.Project,{
            foreignKey: 'idProject',
            allowNull: false
        });

        // ProjectPeriode.belongsTo(models.Person,{
        //     foreignKey: 'idPerson',
        //     allowNull: false
        // });
    };


    return ProjectPeriode;
};