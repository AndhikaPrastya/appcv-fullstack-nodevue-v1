module.exports = (sequelize, DataTypes) => {
    const Role = sequelize.define("Role", {
        idRole: {
            type: DataTypes.STRING(255),
            primaryKey: true,
            allowNull: false
        },
        nameRole: {
            type: DataTypes.STRING(25),
            allowNull: false
        },
        descRole: {
            type: DataTypes.STRING(200),
        }
    });

    Role.associate = models => {
       
        Role.belongsTo(models.Person,{
            foreignKey: 'idPerson',
            allowNull: false
        });

        Role.hasMany(models.ProjectRole,{
            foreignKey:{
            name: 'idRole',
            allowNull: false,
            onDelete: 'cascade',
            onUpdate: 'cascade'
            } 
        });

        Role.hasMany(models.CompanyRole,{
            foreignKey:{
            name: 'idRole',
            allowNull: false,
            onDelete: 'cascade',
            onUpdate: 'cascade'
            } 
        });
    };

    return Role;
};