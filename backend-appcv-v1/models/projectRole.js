module.exports = (sequelize, DataTypes) => {
    const ProjectRole = sequelize.define("ProjectRole", {
        idProjectRole: {
            type: DataTypes.STRING(255),
            primaryKey: true,
            allowNull: false
        },
    });

    ProjectRole.associate = models => {
        ProjectRole.belongsTo(models.Project,{
            foreignKey: 'idProject',
            allowNull: false
        });
        ProjectRole.belongsTo(models.Role,{
            foreignKey: 'idRole',
            allowNull: false
        });
    };


    return ProjectRole;
};