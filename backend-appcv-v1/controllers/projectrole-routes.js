'use strict';

const express = require('express');
const router = express.Router();
const model = require('../models');
const {v4 : uuidv4} = require('uuid');

// GET ProjectRole listing.
router.get('/', async function(req, res, next) {
        const ProjectRole = await model.ProjectRole.findAll({});
        if (ProjectRole.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': ProjectRole
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});
// GET ProjectRole by id
router.get('/:idProject', async function(req, res, next) {
        const idProject = req.params.idProject;
        const ProjectRole = await model.ProjectRole.findAll({where: {idProject: idProject},
            order: [['createdAt', 'ASC']],
            include: [
                {model: model.Role, attributes: ['nameRole']}, 
            ]
        });
        if (ProjectRole.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': ProjectRole
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});

// POST ProjectRole
router.post('/add', async function(req, res, next) {
        var str1 = 'PROROL-';
        const {
            idProject,
            idRole
        } = req.body;
        const idProjectRole = str1.concat(uuidv4());
        if(req.body.idProject==="" || req.body.idProject===null||
            req.body.idRole==="" || req.body.idRole===null
        ) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Project Role cannot be Null',
            })
        } else {
            await model.ProjectRole.create({
                idProjectRole,
                idProject,
                idRole
            }).then(ProjectRole => {
                    if (ProjectRole) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Project Role values added successfully',
                            'values': ProjectRole,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add Project Role',
                        })
                    }
                })}
});
// UPDATE ProjectRole
router.patch('/update/:idProjectRole', async function(req, res, next) {
        const idProjectRole = req.params.idProjectRole;
        const {
            idProject,
            idRole
        } = req.body;
        if(req.body.idProject==="" || req.body.idProject===null||
            req.body.idRole==="" || req.body.idRole===null
        ) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Project role cannot be Null',
            })
        } else {
            await model.ProjectRole.update({
                idProject,
                idRole,
            }, {
            where: {
                idProjectRole: idProjectRole
            }
            }).then(ProjectRole => {
                    if (ProjectRole) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Project Role values added successfully',
                            'values': ProjectRole,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add Project Role',
                        })
                    }
                })}
});
// DELETE ProjectRole
router.delete('/delete/:idProjectRole', async function(req, res, next) {
        const idProjectRole = req.params.idProjectRole;
        const ProjectRole = await model.ProjectRole.destroy({ where: {
        idProjectRole: idProjectRole 
        }})
        if (ProjectRole) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'Project Role values has been deleted successfully',
            'values': ProjectRole,
        })
        } else {
        res.status(204).json({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});

module.exports = router;