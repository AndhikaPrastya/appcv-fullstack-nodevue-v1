'use strict';

const express = require('express');
const router = express.Router();
const model = require('../models');
const {v4 : uuidv4} = require('uuid');

// GET CompanyRole listing.
router.get('/', async function(req, res, next) {
        const CompanyRole = await model.CompanyRole.findAll({});
        if (CompanyRole.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': CompanyRole
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});
// GET CompanyRole by idCOmpany
router.get('/:idCompany', async function(req, res, next) {
        const idCompany = req.params.idCompany;
        const CompanyRole = await model.CompanyRole.findAll({where: {idCompany: idCompany},
            order: [['createdAt', 'ASC']],
            include: [
                {model: model.Role, attributes: ['nameRole']}, 
            ]
        });
        if (CompanyRole.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': CompanyRole
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});

// POST CompanyRole
router.post('/add', async function(req, res, next) {
        var str1 = 'COMROL-';
        const {
            idCompany,
            idRole
        } = req.body;
        const idCompanyRole = str1.concat(uuidv4());
        if(req.body.idCompany==="" || req.body.idCompany===null||
            req.body.idRole==="" || req.body.idRole===null
        ) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Company Role cannot be Null',
            })
        } else {
            await model.CompanyRole.create({
                idCompanyRole,
                idCompany,
                idRole
            }).then(CompanyRole => {
                    if (CompanyRole) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Company Role values added successfully',
                            'values': CompanyRole,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add Company Role',
                        })
                    }
                })}
});

// Update CompanyRole
router.patch('/update/:idCompanyRole', async function(req, res, next) {
        const idCompanyRole = req.params.idCompanyRole;
        const {
            idCompany,
            idRole
        } = req.body;
        if(req.body.idCompany==="" || req.body.idCompany===null||
            req.body.idRole==="" || req.body.idRole===null
        ) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Company role cannot be Null',
            })
        } else {
            await model.CompanyRole.update({
                idCompany,
                idRole,
            }, {
            where: {
                idCompanyRole: idCompanyRole
            }
            }).then(CompanyRole => {
                    if (CompanyRole) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Company Role values added successfully',
                            'values': CompanyRole,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add Company Role',
                        })
                    }
                })}
});

// DELETE CompanyRole
router.delete('/delete/:idCompanyRole', async function(req, res, next) {
        const idCompanyRole = req.params.idCompanyRole;
        const CompanyRole = await model.CompanyRole.destroy({ where: {
        idCompanyRole: idCompanyRole
        }})
        if (CompanyRole) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'Company Role values has been deleted successfully',
            'values': CompanyRole,
        })
        } else {
        res.status(204).json({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});

module.exports = router;