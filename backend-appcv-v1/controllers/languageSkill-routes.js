'use strict';

const express = require('express');
const router = express.Router();
const model = require('../models');
const {v4 : uuidv4} = require('uuid');

// GET LanguageSkill listing.
router.get('/', async function(req, res) {

        const LanguageSkill = await model.LanguageSkill.findAll({});
        if (LanguageSkill.length !== 0) {
        res.status(200).send({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': LanguageSkill
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }
});

// GET LanguageSkill by idPerson
router.get('/:idPerson', async function(req, res) {

        const idPerson = req.params.idPerson;
        const LanguageSkill = await model.LanguageSkill.findAll({where: {idPerson: idPerson},
            order: [['createdAt', 'ASC']],
        });
        if (LanguageSkill.length !== 0) {
        res.status(200).send({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': LanguageSkill
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }
 
});

// GET LanguageSkill by idPerson
router.get('/idLanguageSkill/:idLanguageSkill', async function(req, res) {

        const idLanguageSkill = req.params.idLanguageSkill;
        const LanguageSkill = await model.LanguageSkill.findAll({where: {idLanguageSkill: idLanguageSkill},
            order: [['createdAt', 'ASC']],
        });
        if (LanguageSkill.length !== 0) {
        res.status(200).send({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': LanguageSkill
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }
  
});

// POST LanguageSkill
router.post('/add', async function(req, res) {

        var str1 = 'LAS-';
        const {
            language,
            level,
            idPerson
        } = req.body;
        const idLanguageSkill = str1.concat(uuidv4());
        if(req.body.language==="" || req.body.language===null ||req.body.level==="" || req.body.level===null ) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Language Skill canot be Null',
            })
        } else {
            await model.LanguageSkill.create({
                idLanguageSkill,
                language,
                level,
                idPerson
        }).then(LanguageSkill => {
            if (LanguageSkill) {
                res.status(201).json({
                    'status': 'OK',
                    'messages': 'Language Skill values added successfully',
                    'values': LanguageSkill,
                })
            } else {
                res.send({
                    'status': 'Error',
                    'messages': 'Failed add Language Skill',
                })
            }
        })
        }
   
});

// UPDATE LanguageSkill
router.patch('/update/:idLanguageSkill', async function(req, res) {
   
        const idLanguageSkill = req.params.idLanguageSkill;
        const { 
            language,
            level
        } = req.body;
        if(req.body.language==="" || req.body.language===null ||req.body.level==="" || req.body.level===null ) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Language Skill canot be Null',
            })
        } else {
            await model.LanguageSkill.update({
                language,
                level
                }, {
                where: {
                    idLanguageSkill: idLanguageSkill
                }
            }).then(LanguageSkill => {
            if (LanguageSkill) {
                res.status(201).json({
                    'status': 'OK',
                    'messages': 'Language Skill values added successfully',
                    'values': LanguageSkill,
                })
            } else {
                res.send({
                    'status': 'Error',
                    'messages': 'Failed add Language Skill',
                })
            }
        })
        }
  
});
// DELETE LanguageSkill
router.delete('/delete/:idLanguageSkill', async function(req, res) {

        const idLanguageSkill = req.params.idLanguageSkill;
        const LanguageSkill = await model.LanguageSkill.destroy({ where: {
        idLanguageSkill: idLanguageSkill
        }})
       if (LanguageSkill) {
            res.status(200).json({
            'status': 'OK',
            'messages': 'Language Skill values added successfully',
            'values': LanguageSkill,
            })
        } else {
        res.status(204).json({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }
});

module.exports = router;