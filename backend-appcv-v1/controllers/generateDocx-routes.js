'use strict';

const express = require('express');
const router = express.Router();
const docx = require('docx');
const model = require('../models');
var moment = require('moment');
const fs = require('fs');
const { ShadingType } = require('docx');

router.get("/:idPerson", async (req, res) => {

        const idPerson = req.params.idPerson;
        const Person = await model.Person.findAll({where: {idPerson: idPerson},
            include: [
                {model: model.LanguageSkill, attributes: ['language','level']},
                {model: model.Objective, attributes: ['descObjective']}, 
                {model: model.Profile, attributes: ['descProfile']}, 
                {model: model.Education, attributes: ['school','degree','subject','priodFrom','priodTo']}, 
                {model: model.Course, attributes: ['titleCourse','providerCourse','placeCourse','dateCourse','durationCourse','certificateCourse']}, 
                {model: model.Role, attributes: ['nameRole','descRole']}, 
                {model: model.Company, attributes: ['nameCompany','statusEmployee','addressCompany','contactCompany','priodFrom','priodTo','createdAt'],
                    order: [['createdAt', 'ASC']],
                    include: [
                                {model: model.CompanyRole, attributes: ['idRole'],
                                    include: [
                                            {model: model.Role, attributes: ['nameRole']},
                                    ]
                                }, 
                    ]
                },
            ]
        });

        const ProjectPeriode = await model.ProjectPeriode.findAll({
            order: [['createdAt', 'DESC']],
            include: [
                {model: model.Project, where: {idPerson: idPerson},
                    order: [['createdAt', 'ASC']],
                    include: [
                        {model: model.ProjectRole, attributes: ['idRole'],
                            order: [['createdAt', 'ASC']],
                            include: [
                                    {model: model.Role, attributes: ['nameRole']},
                            ]}, 
                    ]
                }]
        });



        const itemsPerson = Person[0].dataValues;
        const itemsLanguageSkill = []
        const itemsObjective = []
        const itemsProfile = []
        const itemsEducation = []
        const itemsCourse = []
        const itemsRole = []
        const itemsCompany = []
        const itemsCompanyRole = [[]]
        
        for (let i=0; i < Person.length; i++){

            for (let k=0; k < Person[i].LanguageSkills.length; k++){
                itemsLanguageSkill[k] = Person[i].LanguageSkills[k].dataValues
            }
            for (let l=0; l < Person[i].Objectives.length; l++){
                itemsObjective[l] = Person[i].Objectives[l].dataValues
            }
            for (let m=0; m < Person[i].Profiles.length; m++){
                itemsProfile[m] = Person[i].Profiles[m].dataValues
            }
            for (let n=0; n < Person[i].Education.length; n++){
                itemsEducation[n] = Person[i].Education[n].dataValues
            }
            for (let o=0; o < Person[i].Courses.length; o++){
                itemsCourse[o] = Person[i].Courses[o].dataValues
            }
            for (let p=0; p < Person[i].Roles.length; p++){
                itemsRole[p] = Person[i].Roles[p].dataValues
            }
            for (let q=0; q < Person[i].Companies.length; q++){
                itemsCompany[q] = Person[i].Companies[q].dataValues

                for (let qr=0; qr < itemsCompany[q].CompanyRoles.length; qr++) {
                    itemsCompanyRole[q,qr] = itemsCompany[q].CompanyRoles[qr].dataValues.Role.dataValues
                }

            }
                
        }

        const itemsProjectPeriode = []

        for (let i=0; i < ProjectPeriode.length; i++){
            itemsProjectPeriode[i] = ProjectPeriode[i].dataValues
        }

        console.log('Data Project Periode = ',itemsProjectPeriode[0].Project.dataValues.ProjectRoles[0].dataValues.Role.dataValues.nameRole);

        const { Header, Footer, AlignmentType, Document, HeadingLevel, Packer, Paragraph,ShadingType, TabStopPosition, TabStopType, TextRun, ImageRun, Table, TableCell, TableRow, BorderStyle, WidthType, TableBorders } = docx;
        const photo_url = itemsPerson.photoPerson;
        const photo_split = photo_url.split('/');
        const photo = photo_split[6];
        var date = Date.now();


            class DocumentCreator {
                create([itemsObjective, itemsProfile, itemsEducation]) {
                    const document = new Document({
                        sections: [{
                            headers: {
                                default: new Header({
                                    children: [
                                        this.createTitleHeaderTop("Curriculum Vitae | April 2021"),
                                    ],
                                }),
                            },
                            footers: {
                                default: new Header({
                                    children: [
                                        this.createTitleFooterBottom("Page"),
                                    ],
                                }),
                            },
                            children: [
                                new Paragraph({
                                    children: [
                                        new TextRun({
                                            text: itemsPerson.namePerson,
                                            bold: true,
                                            font: "calibri",
                                            size: 35,
                                        }),
                                    ]
                                }),
                                new Table({
                                    borders: TableBorders.NONE,
                                    rows: [
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 1300,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "[Office]:",
                                                                bold: true,
                                                                font: "calibri",
                                                                size: 23,
                                                            }),
                                                        ],}),],
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 10000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [this.createPhoto(photo)],
                                                    rowSpan: 7,
                                                }),
                                            ],
                                        }),
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    children: [new Paragraph({
                                                        children: [
                                                            new TextRun({
                                                                text: `${itemsCompany[0].nameCompany}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ]})],
                                                }),
                                            ],
                                        }),
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    children: [new Paragraph({
                                                        children: [
                                                            new TextRun({
                                                                text: `${itemsCompany[0].addressCompany}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ]})],
                                                }),
                                            ],
                                        }),
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    children: [new Paragraph('\n')],
                                                }),
                                            ],
                                        }),
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    children: [new Paragraph('\n')],
                                                }),
                                            ],
                                        }),
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 1300,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "[Contact]:",
                                                                bold: true,
                                                                font: "calibri",
                                                                size: 23,
                                                            }),
                                                        ],}),],
                                                }),
                                            ],
                                        }),
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    children: [new Paragraph({
                                                        children: [
                                                            new TextRun({
                                                                text: `${itemsCompany[0].contactCompany}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ]})],
                                                }),
                                            ],
                                        }),
                                    ],
                                }),
                        
                                this.createSpace(),
                                new Table({
                                    // borders: TableBorders.NONE,
                                    rows: [
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 11000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({
                                                        children: [
                                                            new TextRun({
                                                                text: "[Personal Detail]:",
                                                                bold: true,
                                                                font: "calibri",
                                                                size: 23,
                                                            }),
                                                        ], })],
                                                    columnSpan: 4,
                                                    shading: {
                                                        fill: "d3d3d3",
                                                        val: ShadingType.PERCENT_90,
                                                        color: "d3d3d3",
                                                    },
                                                    borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }),
                                            ],
                                            
                                        }),
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 1500,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({
                                                        children: [
                                                            new TextRun({
                                                                text: "Place of Birth:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({
                                                        children: [
                                                            new TextRun({
                                                                text: `${itemsPerson.birthPlace}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ]})],
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 1300,
                                                        type: WidthType.DXA,
                                                    },
                                                
                                                    children: [new Paragraph({
                                                        children: [
                                                            new TextRun({
                                                                text: "Gendre:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                        
                                                        children: [new Paragraph({
                                                        children: [
                                                            new TextRun({
                                                                text: `${itemsPerson.gendre}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ]})],
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }),         
                                            ],
                                        }),
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    children: [new Paragraph({
                                                        children: [
                                                            new TextRun({
                                                                text: "Date of Birth:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }),
                                                new TableCell({
                                                    
                                                    children: [new Paragraph({
                                                        children: [
                                                            new TextRun({
                                                                text: `${itemsPerson.birthDate}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ]})],
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }),
                                                new TableCell({
                                                    children: [new Paragraph({
                                                        children: [
                                                            new TextRun({
                                                                text: "Health:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }),
                                                new TableCell({
                                                    children: [new Paragraph({
                                                        children: [
                                                            new TextRun({
                                                                text: `${itemsPerson.health}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ]
                                                        })],
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }), 
                                            ],
                                        }),
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    children: [new Paragraph({
                                                        children: [
                                                            new TextRun({
                                                                text: "Religion:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }),
                                                new TableCell({
                                                    
                                                    children: [new Paragraph({
                                                        children: [
                                                            new TextRun({
                                                                text: `${itemsPerson.religion}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ]})],
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }),
                                                new TableCell({
                                                    children: [new Paragraph({
                                                        children: [
                                                            new TextRun({
                                                                text: "Language:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }),
                                                new TableCell({
                                                    children: [
                                                        ...itemsLanguageSkill
                                                        .map((itemsLanguageSkill) => {
                                                            const arr = [];
                                                            arr.push(
                                                                this.createLanguage(itemsLanguageSkill.language, itemsLanguageSkill.level),
                                                            );

                                                            return arr;
                                                        })
                                                        .reduce((prev, curr) => prev.concat(curr), []),
                                                    ],
                                                     borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }), 
                                            ],
                                        }),
                                    ],
                                }),
                                
                                this.createSpace(),
                                this.createTitleHeader("A. OBJECTIVE"),
                                ...itemsObjective
                                    .map((itemsObjective) => {
                                        const arr = [];
                                        arr.push(
                                            this.createObjective(itemsObjective.descObjective),
                                        );

                                        return arr;
                                    })
                                    .reduce((prev, curr) => prev.concat(curr), []),

                                this.createSpace(),
                                this.createTitleHeader("B. PROFILE"),
                                ...itemsProfile
                                    .map((itemsProfile) => {
                                        const arr = [];
                                        arr.push(
                                            this.createProfile(itemsProfile.descProfile),
                                        );

                                        return arr;
                                    })
                                    .reduce((prev, curr) => prev.concat(curr), []),

                                this.createSpace(),
                                this.createTitleHeader("C. EDUCATION"),
                                this.createSpace(),
                                new Table({
                                    // borders: TableBorders.NONE,
                                    rows: [
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 1300,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "School",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 1300,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Degree",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 1300,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Subject",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 1300,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "From",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 1300,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "To",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                            ],
                                        }),
                                        
                                        ...itemsEducation
                                            .map((itemsEducation) => {
                                                const arr = [];
                                                arr.push(
                                                    this.createTableEducation(itemsEducation.school,itemsEducation.degree,itemsEducation.subject,itemsEducation.priodFrom,itemsEducation.priodTo),
                                                );

                                                return arr;
                                            })
                                            .reduce((prev, curr) => prev.concat(curr), []),
                                    ],
                                }),

                                
                                this.createSpace(),
                                this.createTitleHeaderMostRecent("D. COURSE,TRAINING"),
                                this.createSpace(),
                                new Table({
                                // borders: TableBorders.NONE,
                                rows: [
                                    new TableRow({
                                        children: [
                                            new TableCell({
                                                width: {
                                                    size: 1300,
                                                    type: WidthType.DXA,
                                                },
                                                children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Title",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                            }),
                                            new TableCell({
                                                width: {
                                                    size: 1300,
                                                    type: WidthType.DXA,
                                                },
                                                children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Provider",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                            }),
                                            new TableCell({
                                                width: {
                                                    size: 1300,
                                                    type: WidthType.DXA,
                                                },
                                                children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Place",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                            }),
                                            new TableCell({
                                                width: {
                                                    size: 1300,
                                                    type: WidthType.DXA,
                                                },
                                               children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Date",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                            }),
                                            new TableCell({
                                                width: {
                                                    size: 1300,
                                                    type: WidthType.DXA,
                                                },
                                                children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Duration",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                            }),
                                            new TableCell({
                                                width: {
                                                    size: 1300,
                                                    type: WidthType.DXA,
                                                },
                                                children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Certificate",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        columnSpan: 2,
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                            }),
                                        ],
                                    }),

                                    ...itemsCourse
                                        .map((itemsCourse) => {
                                            const arr = [];
                                            arr.push(
                                                this.createTableCourse(itemsCourse.titleCourse,itemsCourse.providerCourse,itemsCourse.placeCourse,itemsCourse.dateCourse,itemsCourse.durationCourse,itemsCourse.certificateCourse),
                                            );

                                            return arr;
                                        })
                                        .reduce((prev, curr) => prev.concat(curr), []),
                                    ],
                                }),

                                this.createSpace(),
                                this.createTitleHeaderMostRecent("E. RELEVANT EXPERIENCE & ACCOMPLISHMENT"),
                                // this.createSpace(),
                                ...itemsRole
                                    .map((itemsRole) => {
                                        const arr = [];
                                        arr.push(
                                            this.createRole(itemsRole.nameRole,itemsRole.descRole),
                                        );

                                        return arr;
                                    })
                                    .reduce((prev, curr) => prev.concat(curr), []),

                                this.createSpace(),
                                this.createTitleHeaderMostRecent("F. EMPLOYMENT"),
                                this.createSpace(),
                                this.createTitleHeaderMostRecent("F.1 Company"),
                                this.createSpace(),
                                ...itemsCompany
                                    .map((itemsCompany) => {
                                        const arr = [];
                                        var roleCompany = "";

                                        itemsCompany.CompanyRoles.forEach((point) => {
                                            roleCompany = roleCompany+point.Role.nameRole.concat(', ');
                                        })

                                        

                                        console.log(roleCompany);

                                        arr.push(
                                            this.createTableCompany(itemsCompany.nameCompany,itemsCompany.statusEmployee,
                                                                    itemsCompany.priodFrom,itemsCompany.priodTo,roleCompany),
                                        );

                                        return arr;
                                    })
                                    .reduce((prev, curr) => prev.concat(curr), []),

                                
                                this.createSpace(),
                                this.createTitleHeaderMostRecent("F.2 Project"),
                                this.createSpace(),
                                ...itemsProjectPeriode
                                    .map((itemsProjectPeriode) => {
                                        const arr = [];
                                        var roleProject = "";

                                        itemsProjectPeriode.Project.dataValues.ProjectRoles.forEach((point) => {
                                            roleProject = roleProject+point.Role.nameRole.concat(', ');
                                        })

                                        console.log('role project',roleProject);

                                        arr.push(
                                            this.createTableProject( itemsProjectPeriode.Project.nameProject,roleProject,itemsProjectPeriode.priodFrom,itemsProjectPeriode.priodTo,itemsProjectPeriode.Project.customer,itemsProjectPeriode.Project.descProject,itemsProjectPeriode.Project.technicalInfo,itemsProjectPeriode.Project.appType,itemsProjectPeriode.Project.devLanguage,itemsProjectPeriode.Project.serverOs,itemsProjectPeriode.Project.framework,itemsProjectPeriode.Project.database,itemsProjectPeriode.Project.devTool,itemsProjectPeriode.Project.appServer,itemsProjectPeriode.Project.otherInfo),
                                        );

                                        return arr;
                                    })
                                    .reduce((prev, curr) => prev.concat(curr), []),    
                                
                                // end
                            ],
                        }],
                    });
                    


                    return document;
                }


// Function-function

                
                createTitleHeaderTop(text) {
                    return new Table({
                                    // borders: TableBorders.NONE,
                                    rows: [
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 10000,
                                                        type: WidthType.DXA,
                                                    },
                                                    spacing: {
                                                        after: 50,
                                                    },
                                                    
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: text,
                                                                bold: true,
                                                                font: "calibri",
                                                                size: 34,
                                                                color: "a9a9a9"
                                                                
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT
                                                    }),],
                                                        columnSpan: 2,
                                                        
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                        borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 10,
                                                                color: "d3d3d3",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }),
                                            ],
                                        }),
                                        
                                    ],
                                })
                            }
                createTitleFooterBottom(text) {
                    return new Table({
                                    // borders: TableBorders.NONE,
                                    rows: [
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 10000,
                                                        type: WidthType.DXA,
                                                    },
                                                    spacing: {
                                                        after: 50,
                                                    },
                                                    
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: text,
                                                                bold: true,
                                                                font: "calibri",
                                                                size: 20,
                                                                color: "a9a9a9"
                                                                
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT
                                                    }),],
                                                        columnSpan: 2,
                                                        
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                        borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 10,
                                                                color: "d3d3d3",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }),
                                            ],
                                        }),
                                        
                                    ],
                                })
                            }

                createTitleHeader(text) {
                    return new Table({
                                    // borders: TableBorders.NONE,
                                    rows: [
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 10000,
                                                        type: WidthType.DXA,
                                                    },
                                                    spacing: {
                                                        after: 50,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: text,
                                                                bold: true,
                                                                font: "calibri",
                                                                size: 23,
                                                            }),
                                                        ],}),],
                                                        columnSpan: 2,
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                        borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }),
                                            ],
                                        }),
                                        
                                    ],
                                })
                            }
                
                createTitleHeaderMostRecent(text) {
                    return new Table({
                                    // borders: TableBorders.NONE,
                                    rows: [
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 10000,
                                                        type: WidthType.DXA,
                                                    },
                                                    spacing: {
                                                        after: 50,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: text,
                                                                bold: true,
                                                                font: "calibri",
                                                                size: 23,
                                                            }),
                                                        ]
                                                        }),],
                                                        // columnSpan: 2,
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                        borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 10000,
                                                        type: WidthType.DXA,
                                                    },
                                                    spacing: {
                                                        after: 50,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "(From the most recent)",
                                                                italics: true,
                                                                font: "calibri",
                                                                size: 17,
                                                            }),
                                                        ],alignment: AlignmentType.RIGHT,}),],
                                                        columnSpan: 5,
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                        borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        }
                                                }),
                                            ],
                                        }),
                                        
                                    ],
                                })
                            }
                            

                createTableAddress(addressCompany) {
                    return new Table({
                        // borders: TableBorders.NONE,
                        rows: [
                            new TableRow({
                                children: [
                                    new TableCell({
                                        width: {
                                            size: 3000,
                                            type: WidthType.DXA,
                                        },
                                        children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: addressCompany,
                                                                bold: true,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],}),],
                                    }),             
                                ],
                            }),
                        ],
                    });
                }

                createLanguage(language,level) {
                    return new Paragraph({
                        children: [
                            new TextRun({
                                text: language+'('+level+')',
                                bold: false,
                                font: "calibri",
                                size: 20,
                            }),
                        ],
                    });
                }

                createObjective(desc) {
                    return new Paragraph({
                        alignment: AlignmentType.LEFT,
                        children: [
                            new TextRun({
                                text: desc,
                                font: "calibri",
                                size: 20,
                            }),
                        ],
                    });
                }


                createProfile(desc) {
                    return new Paragraph({
                        children: [
                            new TextRun({
                                text: '● ',
                                font: "calibri",
                                size: 20,
                            }),
                            new TextRun({
                                text: `\n${desc}`,
                                font: "calibri",
                                size: 20,
                            }),
                        ],
                    });
                }


                createTableEducation(school,degree,subject,priodFrom,priodTo) {
                    return new TableRow({
                                children: [
                                    new TableCell({
                                        children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: school,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                    }),
                                    new TableCell({
                                        children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: degree,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                    }), 
                                    new TableCell({
                                        children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: subject,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                    }),
                                    new TableCell({
                                        children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: priodFrom,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                    }),
                                    new TableCell({
                                       children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${priodTo}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                    }),
                                ],
                    });
                }

                createTableCourse(titleCourse,providerCourse,placeCourse,dateCourse,durationCourse,certificateCourse) {
                    return new TableRow({
                                children: [
                                    new TableCell({
                                        children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${titleCourse}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                    }),
                                    new TableCell({
                                        children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${providerCourse}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                    }), 
                                    new TableCell({
                                        children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${placeCourse}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                    }),
                                    new TableCell({
                                        children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${dateCourse}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                    }),
                                    new TableCell({
                                        children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${durationCourse}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                    }),
                                    new TableCell({
                                        children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${certificateCourse}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                    }),
                                ],
                    });
                }

                createRole(nameRole,descRole) {
                    return new Table({
                                    borders: TableBorders.NONE,
                                    rows: [
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 10000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: '['+`\n${nameRole}`+']',
                                                                bold: true,
                                                                font: "calibri",
                                                                size: 21,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    
                                                }),
                                            ],
                                        }),
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 10000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: '['+`\n${descRole}`+']',
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    
                                                }),  
                                            ],
                                        }),
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 10000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: '\t',
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    
                                                }),  
                                            ],
                                        }),
                                    ]
                    });
                }

                createTableCompany(nameCompany,statusEmployee,priodFrom,priodTo,role) {
                    return new Table({
                                    // borders: TableBorders.NONE,
                                    rows: [
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: 'Name :',
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${nameCompany}`+' - '+`\n${statusEmployee}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    
                                                    columnSpan: 5,
                                                }),
                                            ],
                                        }),  
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Priod From :",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${priodFrom}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "To :",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${priodTo}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Current Role :",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: role,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                            ],
                                        }),
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({text: '\n', heading: HeadingLevel.HEADING_5, alignment: AlignmentType.RIGHT})],
                                                    borders: {
                                                            top: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    columnSpan: 6,
                                                }),
                                                
                                            ],
                                        }),   
                                    ]
                    });
                }

                createCompanyRole(text) {
                    return new Paragraph({
                        text: text
                    });
                }


                createTableProject(nameProject,role,priodFrom,priodTo,customer,descProject,technicalInfo,appType,devLanguage,serverOs,framework,database,devTool,appServer,otherInfo) {
                    return new Table({
                                    // borders: TableBorders.NONE,
                                    rows: [
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Project Name:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${nameProject}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                   
                                                    columnSpan: 5,
                                                }),
                                            ],
                                        }),  
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Role:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${role}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    
                                                    
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Period From:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${priodFrom}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "To:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${priodTo}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    
                                                }),
                                            ],
                                        }),
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Customer:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${customer}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    
                                                    columnSpan: 5,
                                                }),
                                            ],
                                        }),  
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Project Desc:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${descProject}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    
                                                    columnSpan: 5,
                                                }),
                                            ],
                                        }),  
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Technical Info:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${technicalInfo}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                   
                                                    columnSpan: 5,
                                                }),
                                            ],
                                        }),  
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "App Type:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({text: `\n${appType}`})],
                                                    columnSpan: 2,
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Dev Language:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${devLanguage}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                   
                                                    columnSpan: 2,
                                                }),
                                            ],
                                        }),  
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Server OS:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${serverOs}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    
                                                    columnSpan: 2,
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Framework:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${framework}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    
                                                    columnSpan: 2,
                                                }),
                                            ],
                                        }), 
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "DB:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${database}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    
                                                    columnSpan: 2,
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                   children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Dev Tool:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${devTool}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    
                                                    columnSpan: 2,
                                                }),
                                            ],
                                        }),  
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "App Server:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${appServer}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    
                                                    columnSpan: 2,
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                    children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: "Other Info:",
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.RIGHT,})],
                                                        shading: {
                                                            fill: "d3d3d3",
                                                            val: ShadingType.PERCENT_90,
                                                            color: "d3d3d3",
                                                        },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                }),
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n${otherInfo}`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                   
                                                    columnSpan: 2,
                                                }),
                                            ],
                                        }), 
                                        new TableRow({
                                            children: [
                                                new TableCell({
                                                    width: {
                                                        size: 2000,
                                                        type: WidthType.DXA,
                                                    },
                                                     children: [new Paragraph({children: [
                                                            new TextRun({
                                                                text: `\n`,
                                                                bold: false,
                                                                font: "calibri",
                                                                size: 20,
                                                            }),
                                                        ],
                                                        alignment: AlignmentType.LEFT,})],
                                                        // shading: {
                                                        //     fill: "d3d3d3",
                                                        //     val: ShadingType.PERCENT_90,
                                                        //     color: "d3d3d3",
                                                        // },
                                                         borders: {
                                                            top: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            bottom: {
                                                                style: BorderStyle.SINGLE,
                                                                size: 5,
                                                                color: "a9a9a9",
                                                            },
                                                            right: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                            left: {
                                                                style: BorderStyle.NONE,
                                                                size: 5,
                                                                color: "d3d3d3",
                                                            },
                                                        },
                                                    columnSpan: 6,
                                                }),
                                                
                                            ],
                                        }),    
                                    ]
                    });
                }


                createHeading(text) {
                    return new Paragraph({
                        text: text,
                        heading: HeadingLevel.HEADING_1,
                        thematicBreak: true,
                    });
                }

                createSubHeading(text) {
                    return new Paragraph({
                        text: text,
                        heading: HeadingLevel.HEADING_3,
                        thematicBreak: true,
                    });
                }

                createSpace() {
                    return new Paragraph({
                        text: '\t',
                    });
                }

                createTitle(text) {
                    return new Paragraph({
                        tabStops: [
                            {
                                type: TabStopType.RIGHT,
                                position: TabStopPosition.MAX,
                            },
                        ],
                        text: text,
                        heading: HeadingLevel.HEADING_2,
                        thematicBreak: true,
                        alignment: AlignmentType.RIGHT,
                    });
                }

                createPhoto(photo) {
                    return new Paragraph({
                        tabStops: [
                            {
                                type: TabStopType.RIGHT,
                                position: TabStopPosition.MAX,
                            },
                        ],
                        alignment: AlignmentType.RIGHT,
                        children: [
                            new ImageRun({
                                data: fs.readFileSync('./assets/'+photo),
                                alignment: AlignmentType.RIGHT,
                                transformation: {
                                    width: 100,
                                    height: 120,
                                },
                            }),
                            
                        ],
                    });
                }
            }


            const documentCreator = new DocumentCreator();
            const doc = documentCreator.create([itemsObjective,itemsProfile,itemsEducation]);

            const b64string = await Packer.toBase64String(doc);
                
            res.setHeader('Content-Disposition', 'attachment; filename=My Document.docx');
            res.send(Buffer.from(b64string, 'base64'));
    

});

module.exports = router;