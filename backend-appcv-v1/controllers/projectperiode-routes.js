'use strict';

const express = require('express');
const router = express.Router();
const model = require('../models');
const {v4 : uuidv4} = require('uuid');

// GET ProjectPeriode listing.
router.get('/:idPerson', async function(req, res, next) {
        const idPerson = req.params.idPerson;
        const ProjectPeriode = await model.ProjectPeriode.findAll({
            order: [['createdAt', 'DESC']],
            include: [
                {model: model.Project, where: {idPerson: idPerson},
                    order: [['createdAt', 'ASC']],
                    include: [
                        {model: model.ProjectRole, attributes: ['idRole'],
                            order: [['createdAt', 'ASC']],
                            include: [
                                    {model: model.Role, attributes: ['nameRole']},
                            ]}, 
                    ]
                }]
        });
        if (ProjectPeriode.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': ProjectPeriode
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});
// GET ProjectPeriode by idProjectPeriode
router.get('/idProjectPeriode/:idProjectPeriode', async function(req, res, next) {
        const idProjectPeriode = req.params.idProjectPeriode;
        const ProjectPeriode = await model.ProjectPeriode.findAll({where: {idProjectPeriode: idProjectPeriode}
        });
        if (ProjectPeriode.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': ProjectPeriode
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});

// POST ProjectPeriode
router.post('/add', async function(req, res, next) {
        var str1 = 'PROPRE-';
        const {
            idProject,
            priodFrom,
            priodTo,
        } = req.body;
        const idProjectPeriode = str1.concat(uuidv4());
        if(req.body.priodFrom==="" || req.body.priodFrom===null) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Project Periode cannot be Null',
            })
        } else {
            await model.ProjectPeriode.create({
                idProjectPeriode,
                idProject,
                priodFrom,
                priodTo,
            }).then(ProjectPeriode => {
                    if (ProjectPeriode) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Project Periode values added successfully',
                            'values': ProjectPeriode,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add Project Periode',
                        })
                    }
                })}
});
// UPDATE ProjectPeriode
router.patch('/update/:idProjectPeriode', async function(req, res, next) {
        const idProjectPeriode = req.params.idProjectPeriode;
        const { 
            idProject,
            priodFrom,
            priodTo
        } = req.body;
        if(req.body.priodFrom==="" || req.body.priodFrom===null) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Project Periode cannot be Null',
            })
        } else {
            await model.ProjectPeriode.update({
                idProject,
                priodFrom,
                priodTo
            }, {
            where: {
                idProjectPeriode: idProjectPeriode
            }
            }).then(ProjectPeriode => {
                    if (ProjectPeriode) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Project Periode values added successfully',
                            'values': ProjectPeriode,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add Project Periode',
                        })
                    }
                })}
});
// DELETE ProjectPeriode
router.delete('/delete/:idProjectPeriode', async function(req, res, next) {
        const idProjectPeriode = req.params.idProjectPeriode;
        const ProjectPeriode = await model.ProjectPeriode.destroy({ where: {
        idProjectPeriode: idProjectPeriode 
        }})
        if (ProjectPeriode) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'Project Periode values has been deleted successfully',
            'values': ProjectPeriode,
        })
        } else {
        res.status(204).json({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});

module.exports = router;
