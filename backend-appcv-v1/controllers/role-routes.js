'use strict';

const express = require('express');
const router = express.Router();
const model = require('../models');
const {v4 : uuidv4} = require('uuid');

// GET Role listing.
router.get('/', async function(req, res, next) {

        const Role = await model.Role.findAll({});
        if (Role.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Role
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }
});
// GET Role by person
router.get('/:idPerson', async function(req, res, next) {

        const idPerson = req.params.idPerson;
        const Role = await model.Role.findAll({where: {idPerson: idPerson},
            order: [['createdAt', 'ASC']],
        });
        if (Role.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Role
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }

});
// GET Role by Role
router.get('/idRole/:idRole', async function(req, res, next) {

        const idRole = req.params.idRole;
        const Role = await model.Role.findAll({where: {idRole: idRole},
            order: [['createdAt', 'ASC']],
        });
        if (Role.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Role
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }
    
});
// POST Role
router.post('/add', async function(req, res, next) {

        var str1 = 'ROL-';
        const {
            nameRole,
            descRole,
            idPerson
        } = req.body;
        const idRole = str1.concat(uuidv4());
        if(req.body.nameRole==="" || req.body.nameRole===null || req.body.descRole==="" || req.body.descRole===null) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Role cannot be Null',
            })
        } else {
            await model.Role.create({
                idRole,
                nameRole,
                descRole,
                idPerson
            }).then(Role => {
                    if (Role) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Role values added successfully',
                            'values': Role,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add Role',
                        })
                    }

                })
            }
});

// UPDATE Role
router.patch('/update/:idRole', async function(req, res, next) {

        const idRole = req.params.idRole;
        const { 
            nameRole,
            descRole
        } = req.body;
        if(req.body.nameRole==="" || req.body.nameRole===null || req.body.descRole==="" || req.body.descRole===null) {
        res.status(500).json({
                'status': 'ERROR',
                'messages': 'Desc Profile canot be Null',
            })
        } else {
            await model.Role.update({
                nameRole,
                descRole
            }, {
            where: {
                idRole: idRole
            }
            }).then(Role => {
                    if (Role) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Role values added successfully',
                            'values': Role,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add Role',
                        })
                    }

                })
            }

});

// DELETE Role
router.delete('/delete/:idRole', async function(req, res, next) {

        const idRole = req.params.idRole;
        const Role = await model.Role.destroy({ where: {
        idRole: idRole
        }, include: [
                {model: model.CompanyRole.destroy({ where: {
                        idRole: idRole
                        }})}, 
                {model: model.ProjectRole.destroy({ where: {
                        idRole: idRole
                        }})}, 
            ]
        })        
        if (Role) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'Role values has been deleted successfully',
            'values': Role,
        })
        } else {
        res.status(204).json({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }
    
});

module.exports = router;