'use strict';

const express = require('express');
const router = express.Router();
const model = require('../models');
const {v4 : uuidv4} = require('uuid');

// GET Company listing.
router.get('/', async function(req, res, next) {
        const Company = await model.Company.findAll({});
        if (Company.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Company
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});
// GET Company by idPerson
router.get('/:idPerson', async function(req, res, next) {
        const idPerson = req.params.idPerson;
        const Company = await model.Company.findAll({where: {idPerson: idPerson},
            order: [['createdAt', 'ASC']],
        });
        if (Company.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Company
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});
// GET Company by idCompany
router.get('/idCompany/:idCompany', async function(req, res, next) {
        const idCompany = req.params.idCompany;
        const Company = await model.Company.findAll({where: {idCompany: idCompany}});
        if (Company.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Company
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});
//GET Company nested role.
router.get('/nested/:idPerson', async function(req, res, next) {
        const idPerson = req.params.idPerson;
        const Company = await model.Company.findAll({where: {idPerson: idPerson},
            order: [['createdAt', 'ASC']],
            include: [
                {model: model.CompanyRole, attributes: ['idRole'],
                    include: [
                            {model: model.Role, attributes: ['nameRole']},
                    ]}, 
            ]
        });
        if (Company.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Company
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});
// POST Company
router.post('/add', async function(req, res, next) {
        var str1 = 'COM-';
        const {
            nameCompany,
            statusEmployee,
            addressCompany,
            contactCompany,
            priodFrom,
            priodTo,
            idPerson
        } = req.body;
        const idCompany = str1.concat(uuidv4());
        if(req.body.nameCompany==="" || req.body.nameCompany===null || 
            req.body.statusEmployee==="" || req.body.statusEmployee===null ||
            req.body.priodFrom==="" || req.body.priodFrom===null 
            ) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Comapny cannot be Null',
            })
        } else {
            await model.Company.create({
                idCompany,
                nameCompany,
                statusEmployee,
                addressCompany,
                contactCompany,
                priodFrom,
                priodTo,
                idPerson
        }).then(Company => {
                    if (Company) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Company values added successfully',
                            'values': Company,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add Company',
                        })
                    }
                })
            }
});
// UPDATE Company
router.patch('/update/:idCompany', async function(req, res, next) {
        const idCompany = req.params.idCompany;
        const { 
            nameCompany,
            statusEmployee,
            addressCompany,
            contactCompany,
            priodFrom,
            priodTo
        } = req.body;
        if(req.body.nameCompany==="" || req.body.nameCompany===null || 
            req.body.statusEmployee==="" || req.body.statusEmployee===null ||
            req.body.priodFrom==="" || req.body.priodFrom===null 
            ) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Comapny cannot be Null',
            })
        } else {
            await model.Company.update({
            nameCompany,
            statusEmployee,
            addressCompany,
            contactCompany,
            priodFrom,
            priodTo
        }, {
        where: {
            idCompany: idCompany
        }
        }).then(Company => {
                    if (Company) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Company values added successfully',
                            'values': Company,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add Company',
                        })
                    }

                })
            }
});
// DELETE Company
router.delete('/delete/:idCompany', async function(req, res, next) {
        const idCompany = req.params.idCompany;
        const Company = await model.Company.destroy({ where: {
        idCompany: idCompany
        }})
        if (Company) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'Company values has been deleted successfully',
            'values': Company,
        })
        } else {
        res.status(204).json({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});

module.exports = router;