'use strict';

const express = require('express');
const router = express.Router();
const model = require('../models');
const {v4 : uuidv4} = require('uuid');

// GET Course listing.
router.get('/', async function(req, res) {
        const Course = await model.Course.findAll({});
        if (Course.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Course
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});
// GET Course by idperson
router.get('/:idPerson', async function(req, res) {
        const idPerson = req.params.idPerson;
        const Course = await model.Course.findAll({where: {idPerson: idPerson},
            order: [['createdAt', 'ASC']],
        });
        if (Course.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Course
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});
// GET Course by idCourse
router.get('/idCourse/:idCourse', async function(req, res) {
        const idCourse = req.params.idCourse;
        const Course = await model.Course.findAll({where: {idCourse: idCourse},
            order: [['createdAt', 'ASC']],
        });
        if (Course.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Course
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});
// POST Course
router.post('/add', async function(req, res) {
        var str1 = 'COU-';
        const {
            titleCourse,
            providerCourse,
            placeCourse,
            dateCourse,
            durationCourse,
            certificateCourse,
            idPerson
        } = req.body;
        const idCourse = str1.concat(uuidv4());
        if(req.body.titelCourse==="" || req.body.titelCourse===null || 
            req.body.providerCourse==="" || req.body.providerCourse===null ||
            req.body.dateCourse==="" || req.body.dateCourse===null ||
            req.body.durationCourse==="" || req.body.durationCourse===null 
            ) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Comapny cannot be Null',
            })
        } else {
            await model.Course.create({
                idCourse,
                titleCourse,
                providerCourse,
                placeCourse,
                dateCourse,
                durationCourse,
                certificateCourse,
                idPerson
        }).then(Course => {
                    if (Course) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Course values added successfully',
                            'values': Course,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add Course',
                        })
                    }
                })}
});
// UPDATE Coursee
router.patch('/update/:idCourse', async function(req, res) {
        const idCourse = req.params.idCourse;
        const { 
            titleCourse,
            providerCourse,
            placeCourse,
            dateCourse,
            durationCourse,
            certificateCourse
        } = req.body;
        if(req.body.titelCourse==="" || req.body.titelCourse===null || 
            req.body.providerCourse==="" || req.body.providerCourse===null ||
            req.body.dateCourse==="" || req.body.dateCourse===null ||
            req.body.durationCourse==="" || req.body.durationCourse===null 
            ) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Comapny cannot be Null',
            })
        } else {
            await model.Course.update({
                titleCourse,
                providerCourse,
                placeCourse,
                dateCourse,
                durationCourse,
                certificateCourse
            }, {
            where: {
                idCourse: idCourse
            }
            }).then(Course => {
                    if (Course) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Course values added successfully',
                            'values': Course,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add Course',
                        })
                    }
                })}
});
// DELETE Course
router.delete('/delete/:idCourse', async function(req, res) {
        const idCourse = req.params.idCourse;
        const Course = await model.Course.destroy({ where: {
        idCourse: idCourse
        }})
        if (Course) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'Course values has been deleted successfully',
            'values': Course,
        })
        } else {
        res.status(204).json({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});

module.exports = router;