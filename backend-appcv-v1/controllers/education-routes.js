'use strict';

const express = require('express');
const router = express.Router();
const model = require('../models');
const {v4 : uuidv4} = require('uuid');

// GET Education listing.
router.get('/', async function(req, res, next) {
        const Education = await model.Education.findAll({});
        if (Education.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Education
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});
// GET Education by person
router.get('/:idPerson', async function(req, res, next) {
        const idPerson = req.params.idPerson;
        const Education = await model.Education.findAll({where: {idPerson: idPerson},
            order: [['createdAt', 'ASC']],
        });
        if (Education.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Education
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});
// GET Education by idEducation
router.get('/idEducation/:idEducation', async function(req, res, next) {
        const idEducation = req.params.idEducation;
        const Education = await model.Education.findAll({where: {idEducation: idEducation},
            order: [['createdAt', 'ASC']],
        });
        if (Education.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Education
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});
// POST Education
router.post('/add', async function(req, res, next) {
        var str1 = 'EDU-';
        const {
            school,
            degree,
            subject,
            priodFrom,
            priodTo,
            idPerson
        } = req.body;
        const idEducation = str1.concat(uuidv4());
        if(req.body.school==="" || req.body.school===null || 
            req.body.degree==="" || req.body.degree===null ||
            req.body.priodFrom==="" || req.body.priodFrom===null 
            ) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Education cannot be Null',
            })
        } else {
            await model.Education.create({
                idEducation,
                school,
                degree,
                subject,
                priodFrom,
                priodTo,
                idPerson
        }).then(Education => {
                    if (Education) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Education values added successfully',
                            'values': Education,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add education',
                        })
                    }
                })}
});
// UPDATE Education
router.patch('/update/:idEducation', async function(req, res, next) {
        const idEducation = req.params.idEducation;
        const { 
            school,
            degree,
            subject,
            priodFrom,
            priodTo
        } = req.body;
        if(req.body.school==="" || req.body.school===null || 
            req.body.degree==="" || req.body.degree===null ||
            req.body.priodFrom==="" || req.body.priodFrom===null 
            ) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Education cannot be Null',
            })
        } else {
            await model.Education.update({
                school,
                degree,
                subject,
                priodFrom,
                priodTo
            }, {
            where: {
                idEducation: idEducation
            }
            }).then(Education => {
                    if (Education) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Education values added successfully',
                            'values': Education,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add education',
                        })
                    }
                })}
});
// DELETE Education
router.delete('/delete/:idEducation', async function(req, res, next) {
        const idEducation = req.params.idEducation;
        const Education = await model.Education.destroy({ where: {
        idEducation: idEducation
        }})
        if (Education) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'Education values has been deleted successfully',
            'values': Education,
        })
        } else {
        res.status(204).json({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}

});

module.exports = router;