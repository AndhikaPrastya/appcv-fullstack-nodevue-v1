'use strict';

const express = require('express');
const router = express.Router();
const model = require('../models');
const {v4 : uuidv4} = require('uuid');

// GET Objective listing.
router.get('/', async function(req, res) {
 
        const Objective = await model.Objective.findAll({})
        if (Objective.length !== 0) {
        res.status(200).send({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Objective
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});

// GET Objective by id
router.get('/:idPerson', async function(req, res, next) {

        const idPerson = req.params.idPerson;
        const Objective = await model.Objective.findAll({where: {idPerson: idPerson},
            order: [['createdAt', 'ASC']],
        });
        if (Objective.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Objective
        })
        } else {
        res.status(204).json({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }

});

// GET Objective by Objective
router.get('/idObjective/:idObjective', async function(req, res) {

        const idObjective = req.params.idObjective;
        const Objective = await model.Objective.findAll({where: {idObjective: idObjective},
            order: [['createdAt', 'ASC']],
        });
        if (Objective.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Objective
        })
        } else {
        res.status(204).json({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }
    
});

// POST Objective
router.post('/add', async function(req, res, err) {

        var str1 = 'OBJ-';
        const {
            descObjective,
            idPerson
        } = req.body;
        const idObjective = str1.concat(uuidv4());
        if(req.body.descObjective==="" || req.body.descObjective===null) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Desc Objective canot be Null',
            })
        } else {
            await model.Objective.create({
            idObjective,
            descObjective,
            idPerson
        }).then(Objective => {
            if (Objective) {
                res.status(201).json({
                    'status': 'OK',
                    'messages': 'Objective values added successfully',
                    'values': Objective,
                })
            } else {
                res.send({
                    'status': 'Error',
                    'messages': 'Failed add objective',
                })
            }

        })
        }
        
});
// UPDATE Objective
router.patch('/update/:idObjective', async function(req, res, err) {
        const idObjective = req.params.idObjective;
        const { 
            descObjective
        } = req.body;
        
        if(req.body.descObjective==="" || req.body.descObjective===null) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Desc Objective canot be Null',
            })
        } else {
            await model.Objective.update({descObjective}, 
                { where: {
                        idObjective: idObjective
                    }
            }).then(Objective => {
                if (Objective) {
                    res.status(200).json({
                        'status': 'OK',
                        'messages': 'Objective values added successfully',
                        'values': Objective,
                    })
                } else {
                    res.send({
                        'status': 'Error',
                        'messages': 'Failed add objective',
                    })
                }

            })
        } 
});
// DELETE Objective
router.delete('/delete/:idObjective', async function(req, res, next) {
        const idObjective = req.params.idObjective;
        const Objective = await model.Objective.destroy({ where: {
        idObjective: idObjective
        }})
        if (Objective) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'Objective values added successfully',
            'values': Objective,
            })
        } else {
        res.status(204).json({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }
});

module.exports = router;