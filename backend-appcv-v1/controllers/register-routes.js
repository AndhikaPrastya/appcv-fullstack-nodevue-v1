'use strict';

const express = require('express');
const router = express.Router();
const model = require('../models');
const {v4 : uuidv4} = require('uuid');
const bcrypt = require('bcryptjs');

// POST Person register
router.post('/', async function(req, res, next) {
        var str1 = 'PER-';
        var salt = bcrypt.genSaltSync(10);
        const password_hash = bcrypt.hashSync(req.body.password, salt);
        const password = password_hash;
        const {
            namePerson,
            birthPlace,
            birthDate,
            gendre,
            health,
            religion,
            email,
        } = req.body;
        const idPerson = str1.concat(uuidv4());
        if(req.body.namePerson==="" || req.body.namePerson===null || 
            req.body.birthPlace==="" || req.body.birthPlace===null ||
            req.body.birthDate==="" || req.body.birthDate===null
            ) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Registration cannot be Null',
            })
        } else {
            const emailCheck =  await model.Person.findAll({where: {email: email}});
            if (emailCheck.length === 0) {
                await model.Person.create({
                    idPerson,
                    namePerson,
                    birthPlace,
                    birthDate,
                    gendre,
                    health,
                    religion,
                    email,
                    password,
                }).then(Person => {
                    if (Person) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Person values added successfully',
                            'values': Person,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add Course',
                        })
                    }
                })
            } else {
                    res.status(401).json({
                        'status': 'ERROR',
                        'messages': 'Email Already Exists',
                        'values': {},
                        })
                }
            }  
});

module.exports = router;