'use strict';

const express = require('express');
const router = express.Router();
const model = require('../models');
const {v4 : uuidv4} = require('uuid');

// GET Profile listing.
router.get('/', async function(req, res) {

        const Profile = await model.Profile.findAll({});
        if (Profile.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Profile
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }
});

// GET Profile by idPerson
router.get('/:idPerson', async function(req, res) {

        const idPerson = req.params.idPerson;
        const Profile = await model.Profile.findAll({where: {idPerson: idPerson},
            order: [['createdAt', 'ASC']],
        });
        if (Profile.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Profile
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }

});

// GET Profile by idProfile
router.get('/idProfile/:idProfile', async function(req, res) {

        const idProfile = req.params.idProfile;
        const Profile = await model.Profile.findAll({where: {idProfile: idProfile},
            order: [['createdAt', 'ASC']],
        });
        if (Profile.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Profile
        })
        } else {
        res.status(204).json({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }

});
// POST Profile
router.post('/add', async function(req, res, err) {

        var str1 = 'PRO-';
        const {
            descProfile,
            idPerson
        } = req.body;
        const idProfile = str1.concat(uuidv4());
        if(req.body.descProfile==="" || req.body.descProfile===null) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Desc Profile canot be Null',
            })
        } else {
            await model.Profile.create({
                idProfile,
                descProfile,
                idPerson
            }).then(profile => {
                    if (profile) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Profile values added successfully',
                            'values': profile,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add profile',
                        })
                    }

                })
            }
});

// UPDATE Profile
router.patch('/update/:idProfile', async function(req, res, err) {
        const idProfile = req.params.idProfile;
        const { 
            descProfile
        } = req.body;
        if(req.body.descProfile==="" || req.body.descProfile===null) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Desc Profile canot be Null',
            })
        } else {
            await model.Profile.update({descProfile}, 
                { where: {
                        idProfile: idProfile
                    }
            }).then(Profile => {
                if (Profile) {
                    res.status(200).json({
                        'status': 'OK',
                        'messages': 'Profile values added successfully',
                        'values': Profile,
                    })
                } else {
                    res.send({
                        'status': 'Error',
                        'messages': 'Failed add Profile',
                    })
                }

            })
        } 
});
// DELETE Profile
router.delete('/delete/:idProfile', async function(req, res, next) {

        const idProfile = req.params.idProfile;
        const Profile = await model.Profile.destroy({ where: {
        idProfile: idProfile
        }})
        if (Profile) {
            res.status(200).json({
            'status': 'OK',
            'messages': 'Profile values has been deleted successfully',
            'values': Profile,
            })
        } else {
        res.status(204).json({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }
  
});

module.exports = router;