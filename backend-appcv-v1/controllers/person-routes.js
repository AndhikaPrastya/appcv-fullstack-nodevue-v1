'use strict';

const express = require('express');
const router = express.Router();
const model = require('../models');
const {v4 : uuidv4} = require('uuid');
const multer = require('multer');

// GET Person listing.
router.get('/', async function(req, res, next) {
        const Person = await model.Person.findAll({});
        if (Person.length !== 0) {
        res.status(200).send({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Person
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});
// GET Person by idPerson
router.get('/:idPerson', async function(req, res, next) {
        const idPerson = req.params.idPerson;
        const Person = await model.Person.findAll({where: {idPerson: idPerson}});
        if (Person.length !== 0) {
        res.status(200).send({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Person
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});

// UPDATE Person
router.patch('/update/:idPerson', async function(req, res, next) {
        const idPerson = req.params.idPerson;
        const {
            namePerson,
            birthPlace,
            birthDate,
            gendre,
            health,
            religion,
        } = req.body;
        if(req.body.namePerson==="" || req.body.namePerson===null || 
            req.body.birthPlace==="" || req.body.birthPlace===null ||
            req.body.birthDate==="" || req.body.birthDate===null
            ) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Registration cannot be Null',
            })
        } else {
            await model.Person.update({
                namePerson,
                birthPlace,
                birthDate,
                gendre,
                health,
                religion,
            }, {
            where: {
                idPerson: idPerson
            }
            }).then(Person => {
                    if (Person) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'erson values has been update successfully',
                            'values': Person,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add Course',
                        })
                    }
                })
        }
        
});

const multerDiskStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './assets');
    },
    filename: function(req, file, cb) {
        const originalName = file.originalname;
        const nameArr = originalName.split('.');
        var extension = '';
        if(nameArr.length > 1) {
            extension = nameArr[nameArr.length - 1];
        }
        cb(null, 'profile'+'-'+Date.now()+'.'+extension);
    }
});

const multerUpload = multer({storage: multerDiskStorage});

// get Photo
router.use('/photo', express.static('assets'));

// UPDATE Photo Profile
router.patch('/upload/:idPerson', multerUpload.single('photo'), async function(req, res, next) {
        const idPerson = req.params.idPerson;
        const photo = req.file;
        const photoPerson = "http://localhost:3000/api/person/photo/"+photo.filename;
        console.log(photo);
        if(!photo) {
            res.status(400).json({'message': 'Picture cannot be empety'});
            return
        }
        // save backend photo
        // res.send(photo);
        // save name directory to db
        await model.Person.update({
            photoPerson 
        }, {
        where: {
            idPerson: idPerson
        }
        });
        // if (Person) {
        res.status(200).send({
            'status': 'OK',
            'messages': 'Upload photo has been update successfully',
            'Profile_url': photoPerson,
        })
});

// router.get('/getphoto/:photoPerson', async function(req, res, next) {
//         const photoPerson = req.params.photoPerson;
//         const Person = await model.Person.findAll({where: {photoPerson: photoPerson}});
//         if (Person.length !== 0) {
//         res.status(200).send({
//             'status': 'OK',
//             'messages': 'SUCCESS',
//             'values': Person
//         })
//         } else {
//         res.status(204).send({
//             'status': 'ERROR',
//             'messages': 'EMPTY',
//             'values': {}
//         })}
// });

// DELETE Person
router.delete('/delete/:idPerson', async function(req, res, next) {
        const idPerson = req.params.idPerson;
        const Person = await model.Person.destroy({ where: {
        idPerson: idPerson
        }, include: [
                {model: model.LanguageSkill.destroy({ where: {
                        idPerson: idPerson
                        }})}, 
                {model: model.Objective.destroy({ where: {
                        idPerson: idPerson
                        }})}, 
                {model: model.Profile.destroy({ where: {
                        idPerson: idPerson
                        }})}, 
                {model: model.Education.destroy({ where: {
                        idPerson: idPerson
                        }})}, 
                {model: model.Course.destroy({ where: {
                        idPerson: idPerson
                        }})}, 
                {model: model.Role.destroy({ where: {
                        idPerson: idPerson
                        }})}, 
                {model: model.Company.destroy({ where: {
                        idPerson: idPerson
                        }})}, 
                {model: model.Project.destroy({ where: {
                        idPerson: idPerson
                        }})}, 
            ]
        })
        if (Person) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'Person values added successfully',
            'values': Person,
            })
        } else {
        res.status(204).json({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })}
});

module.exports = router;