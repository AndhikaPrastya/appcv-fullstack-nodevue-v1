'use strict';

const express = require('express');
const router = express.Router();
const model = require('../models');
const {v4 : uuidv4} = require('uuid');

// GET Project listing.
router.get('/', async function(req, res, next) {

        const Project = await model.Project.findAll({});
        if (Project.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Project
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }
   
});

// GET Project by idPerson
router.get('/:idPerson', async function(req, res, next) {

        const idPerson = req.params.idPerson;
        const Project = await model.Project.findAll({where: {idPerson: idPerson}});
        if (Project.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Project
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }
    
});
// GET Project by idPerson
router.get('/idProject/:idProject', async function(req, res, next) {

        const idProject = req.params.idProject;
        const Project = await model.Project.findAll({where: {idProject: idProject}});
        if (Project.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Project
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }
    
});

//GET project nested role.
router.get('/nested/:idPerson', async function(req, res, next) {

        const idPerson = req.params.idPerson;
        const Project = await model.Project.findAll({where: {idPerson: idPerson},
            order: [['createdAt', 'ASC']],
            include: [
                {model: model.ProjectRole, attributes: ['idRole'],
                    include: [
                            {model: model.Role, attributes: ['nameRole']},
                    ]}, 
            ]
        });
        if (Project.length !== 0) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'SUCCESS',
            'values': Project
        })
        } else {
        res.status(204).send({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }
    
});

// POST Project
router.post('/add', async function(req, res, next) {

        var str1 = 'PRJ-';
        const {
            nameProject,
            customer,
            descProject,
            appType,
            serverOs,
            database,
            appServer,
            framework,
            devTool,
            devLanguage,
            technicalInfo,
            otherInfo,
            idPerson
        } = req.body;
        const idProject = str1.concat(uuidv4());
        if(req.body.nameProject==="" || req.body.nameProject===null || 
            req.body.customer==="" || req.body.customer===null
            ) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Project cannot be Null',
            })
        } else {
            await model.Project.create({
                idProject,
                nameProject,
                customer,
                descProject,
                appType,
                serverOs,
                database,
                appServer,
                framework,
                devTool,
                devLanguage,
                technicalInfo,
                otherInfo,
                idPerson
        }).then(Project => {
                    if (Project) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Project values added successfully',
                            'values': Project,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add Project',
                        })
                    }

                })
            }
    
});

// UPDATE Project
router.patch('/update/:idProject', async function(req, res, next) {

        const idProject = req.params.idProject;
        const { 
            nameProject,
            customer,
            descProject,
            appType,
            serverOs,
            database,
            appServer,
            framework,
            devTool,
            devLanguage,
            technicalInfo,
            otherInfo,
        } = req.body;
        if(req.body.nameProject==="" || req.body.nameProject===null || 
            req.body.customer==="" || req.body.customer===null
            ) {
            res.status(500).json({
                'status': 'ERROR',
                'messages': 'Project cannot be Null',
            })
        } else {
            await model.Project.update({
                nameProject,
                customer,
                descProject,
                appType,
                serverOs,
                database,
                appServer,
                framework,
                devTool,
                devLanguage,
                technicalInfo,
                otherInfo,
            }, {
            where: {
                idProject: idProject
            }
            }).then(Project => {
                    if (Project) {
                        res.status(201).json({
                            'status': 'OK',
                            'messages': 'Project values added successfully',
                            'values': Project,
                        })
                    } else {
                        res.send({
                            'status': 'Error',
                            'messages': 'Failed add Project',
                        })
                    }

                })
            }
   
});
// DELETE Project
router.delete('/delete/:idProject', async function(req, res, next) {

        const idProject = req.params.idProject;
        const Project = await model.Project.destroy({ where: {
        idProject: idProject
        }, include: [
                {model: model.ProjectPeriode.destroy({ where: {
                        idProject: idProject
                        }})}, 
            ]
        })
        if (Project) {
        res.status(200).json({
            'status': 'OK',
            'messages': 'Project values has been deleted successfully',
            'values': Project,
        })
        } else {
        res.status(204).json({
            'status': 'ERROR',
            'messages': 'EMPTY',
            'values': {}
        })
        }

});

module.exports = router;