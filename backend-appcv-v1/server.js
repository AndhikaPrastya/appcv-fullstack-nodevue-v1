'use strict';
// import module express js
const express = require ('express');
const app = express();
const db = require("./models");
const cors = require('cors');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const PORT = process.env.PORT || 3000;

app.use(morgan('combined'))
app.use(bodyParser.json());
app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(express.static('public'));
app.use('/assets', express.static('assets'));
// use cors
app.use(cors());


// call routes
var routes = require('./routes/routes');
routes(app);

db.sequelize.sync().then(()=>{
    app.listen(PORT, () => {
        console.log(`Server started on port ${PORT}`);
    });
});

module.exports = app