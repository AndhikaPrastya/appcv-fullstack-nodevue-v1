const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const {response} = require("express");
let expect = chai.expect;

// Assertion style
chai.should();

chai.use(chaiHttp);

let token = "";
let idCompany = "";
let idCompanyRole = "";

describe('Tasks API Login User (Company Role)', () =>{
    
    /**
     * Test the Login User
     */

    it("User should be in database", (done)=>{

            chai.request(server)
                .post("/api/login")
                .send({
                    'email': 'test@gmail.com',
                    'password': '12345'
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('values').a('object');
                    token = res.body.token;
                    idPerson = res.body.values.idPerson;
                    // console.log(idPerson);
                    done();
                });
        })
});


describe('Tasks API Company Role', () =>{
    
    /**
     * Test the POST reoute
     */

    describe("POST /api/company-role/add", ()=> {

        it("It should POST a new task (Login)", (done)=>{
            const task = {
                idCompany: "COM-7064ad4c-d707-47af-8395-40a471ddb2aa",
                idRole: "ROL-1c022e61-dfda-4a24-ab33-b442c25dcdb6"
            }
            chai.request(server)
                .post("/api/company-role/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                    idCompanyRole = res.body.values.idCompanyRole;
                done();
                });
        })

        it("It should NOT POST in new task because mandatory value has not been entered (Login)", (done)=>{
            const task = {
                idCompany: null,
                idRole: null
            }
            chai.request(server)
                .post("/api/company-role/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })

        it("It should NOT POST a new task (Not Login)", (done)=>{
            const task = {
                idCompany: "COM-7064ad4c-d707-47af-8395-40a471ddb2aa",
                idRole: "ROL-1c022e61-dfda-4a24-ab33-b442c25dcdb6"
            }
            chai.request(server)
                .post("/api/company-role/add")
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the GET reoute
     */

    describe("GET /api/company-role", ()=> {

        it("It should GET all company role (Login)", (done)=>{
            chai.request(server)
                .get("/api/company-role/")
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('messages').eq('SUCCESS');
                    res.body.should.have.property('values').a('array');
                    done();
                });
        })

        it("It should NOT GET all company (Not Login)", (done)=>{
            chai.request(server)
                .get("/api/company-role/")
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the GET by idCompany reoute
     */

    describe("GET /api/company-role/:idCompany", ()=> {

        it("It should GET a task by idCompany (Login)", (done)=>{
            const taskIdCompany = idCompany
            chai.request(server)
                .get("/api/company-role/" + taskIdCompany)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should NOT GET a task by idCompany (Login)", (done)=>{
            const taskIdCompany = "XXX"
            chai.request(server)
                .get("/api/company-role/" + taskIdCompany)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idCompany (Not Login)", (done)=>{
            const taskIdCompany = idCompany
            chai.request(server)
                .get("/api/company-role/" + taskIdCompany)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the PATCH reoute
     */

    describe("PATCH /api/company-role/update/:idCompanyRole", ()=> {

        it("It should PATCH an existing task (Login)", (done)=>{
            const taskIdCompanyRole = idCompanyRole
            const task = {
                idCompany: "COM-7064ad4c-d707-47af-8395-40a471ddb2aa",
                idRole: "ROL-1c022e61-dfda-4a24-ab33-b442c25dcdb6"
            }
            chai.request(server)
                .patch("/api/company-role/update/" + taskIdCompanyRole)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                done();
                });
        })

        it("It should NOT PATCH in new task because mandatory value has not been entered (Login)", (done)=>{
            const taskIdCompanyRole = idCompanyRole
            const task = {
                idCompany: null,
                idRole: null
            }
            chai.request(server)
                .patch("/api/company-role/update/" + taskIdCompanyRole)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })

        it("It should NOT PATCH an existing task (Not Login)", (done)=>{
            const taskIdCompanyRole = idCompanyRole
            const task = {
                idCompany: "COM-7064ad4c-d707-47af-8395-40a471ddb2aa",
                idRole: "ROL-1c022e61-dfda-4a24-ab33-b442c25dcdb6"
            }
            chai.request(server)
                .patch("/api/company-role/update/" + taskIdCompanyRole)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the DELETE reoute
     */

    describe("DELETE /api/company-role/delete/:idCompanyRole", ()=> {

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdCompanyRole = idCompanyRole
            chai.request(server)
                .delete("/api/company-role/delete/" + taskIdCompanyRole)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                });
        })

        it("It should NOT DELETE an existing task (Login)", (done)=>{
            const taskIdCompanyRole = "XXX"
            chai.request(server)
                .delete("/api/company-role/delete/" + taskIdCompanyRole)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT DELETE an existing task (Not Login)", (done)=>{
            const taskIdCompanyRole = idCompanyRole
            chai.request(server)
                .delete("/api/company-role/delete/" + taskIdCompanyRole)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

});