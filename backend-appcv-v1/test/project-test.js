const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const {response} = require("express");
let expect = chai.expect;

// Assertion style
chai.should();

chai.use(chaiHttp);

let token = "";
let idPerson = "";
let idProject = "";

describe('Tasks API Login User (Project)', () =>{
    
    /**
     * Test the Login User
     */

    it("User should be in database", (done)=>{

            chai.request(server)
                .post("/api/login")
                .send({
                    'email': 'test@gmail.com',
                    'password': '12345'
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('values').a('object');
                    token = res.body.token;
                    idPerson = res.body.values.idPerson;
                    // console.log(idPerson);
                    done();
                });
        })
});


describe('Tasks API Project', () =>{
    
    /**
     * Test the POST reoute
     */

    describe("POST /api/project/add", ()=> {

        it("It should POST a new task (Login)", (done)=>{
            const task = {
                nameProject: "Task nameProject",
                customer: "Task customer",
                descProject: "Task descProject",
                appType: "Task appType",
                serverOs: "Task serverOs",
                database: "Task database",
                appServer: "Task appServer",
                framework: "Task framework",
                devTool: "Task devTool",
                devLanguage: "Task devLanguage",
                technicalInfo: "Task technicalInfo",
                otherInfo: "Task otherInfo",
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/project/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                    idProject = res.body.values.idProject;
                done();
                });
        })

        it("It should NOT POST in new task because mandatory value has not been entered (Login)", (done)=>{
            const task = {
                nameProject: null,
                customer: null,
                descProject: null,
                appType: "Task appType",
                serverOs: "Task serverOs",
                database: "Task database",
                appServer: "Task appServer",
                framework: "Task framework",
                devTool: "Task devTool",
                devLanguage: "Task devLanguage",
                technicalInfo: "Task technicalInfo",
                otherInfo: "Task otherInfo",
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/project/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })

        it("It should NOT POST a new task (Not Login)", (done)=>{
            const task = {
                nameProject: "Task nameProject",
                customer: "Task customer",
                descProject: "Task descProject",
                appType: "Task appType",
                serverOs: "Task serverOs",
                database: "Task database",
                appServer: "Task appServer",
                framework: "Task framework",
                devTool: "Task devTool",
                devLanguage: "Task devLanguage",
                technicalInfo: "Task technicalInfo",
                otherInfo: "Task otherInfo",
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/project/add")
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })
    
    
    /**
     * Test the GET reoute
     */

    describe("GET /api/project", ()=> {

        it("It should GET all project (Login)", (done)=>{
            chai.request(server)
                .get("/api/project/")
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('messages').eq('SUCCESS');
                    res.body.should.have.property('values').a('array');
                    done();
                });
        })

        it("It should NOT GET all project (Not Login)", (done)=>{
            chai.request(server)
                .get("/api/project/")
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the GET by idPerson reoute
     */

    describe("GET /api/project/idPerson", ()=> {

        it("It should GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/project/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should NOT GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = "XXXX";
            chai.request(server)
                .get("/api/project/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idPerson (Not Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/project/" + taskIdPerson)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })
    })
    
    
    /**
     * Test the GET by idProject reoute
     */

    describe("GET /api/project/idProject", ()=> {

        it("It should GET a task by idProject (Login)", (done)=>{
            const taskIdProject = idProject
            chai.request(server)
                .get("/api/project/idProject/" + taskIdProject)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should NOT GET a task by idProject (Login)", (done)=>{
            const taskIdProject = "XXXX"
            chai.request(server)
                .get("/api/project/idProject/" + taskIdProject)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idProject (Not Login)", (done)=>{
            const taskIdProject = idProject
            chai.request(server)
                .get("/api/project/idProject/" + taskIdProject)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the GET nested by idPerson reoute
     */

    describe("GET /api/project/nested/:idPerson", ()=> {

        it("It should GET nested a task by idPerson (Login)", (done)=>{
            const taskIdPerson = idPerson
            chai.request(server)
                .get("/api/project/nested/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should NOT GET nested a task by idPerson (Login)", (done)=>{
            const taskIdPerson = "XXXX"
            chai.request(server)
                .get("/api/project/nested/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET nested a task by idPerson (Not Login)", (done)=>{
            const taskIdPerson = idPerson
            chai.request(server)
                .get("/api/project/nested/" + taskIdPerson)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the PATCH reoute
     */

    describe("PATCH /api/project/update/:idProject", ()=> {

        it("It should PATCH an existing task (Login)", (done)=>{
            const taskIdProject = idProject
            const task = {
                nameProject: "Task nameProject Update",
                customer: "Task customer Update",
                descProject: "Task descProject Update",
                appType: "Task appType Update",
                serverOs: "Task serverOs Update",
                database: "Task database Update",
                appServer: "Task appServer Update",
                framework: "Task framework Update",
                devTool: "Task devTool Update",
                devLanguage: "Task devLanguage Update",
                technicalInfo: "Task technicalInfo Update",
                otherInfo: "Task otherInfo Update",
            }
            chai.request(server)
                .patch("/api/project/update/" + taskIdProject)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                done();
                });
        })

        it("It should NOT PATCH in new task because mandatory value has not been entered (Login)", (done)=>{
            const taskIdProject = idProject
            const task = {
                nameProject: null,
                customer: null,
                descProject: null,
                appType: "Task appType Update",
                serverOs: "Task serverOs Update",
                database: "Task database Update",
                appServer: "Task appServer Update",
                framework: "Task framework Update",
                devTool: "Task devTool Update",
                devLanguage: "Task devLanguage Update",
                technicalInfo: "Task technicalInfo Update",
                otherInfo: "Task otherInfo Update",
            }
            chai.request(server)
                .patch("/api/project/update/" + taskIdProject)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })


        it("It should NOT PATCH an existing task (Not Login)", (done)=>{
            const taskIdProject = idProject
            const task = {
                nameProject: "Task nameProject Update",
                customer: "Task customer Update",
                descProject: "Task descProject Update",
                appType: "Task appType Update",
                serverOs: "Task serverOs Update",
                database: "Task database Update",
                appServer: "Task appServer Update",
                framework: "Task framework Update",
                devTool: "Task devTool Update",
                devLanguage: "Task devLanguage Update",
                technicalInfo: "Task technicalInfo Update",
                otherInfo: "Task otherInfo Update",
            }
            chai.request(server)
                .patch("/api/project/update/" + taskIdProject)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the DELETE reoute
     */

    describe("DELETE /api/project/delete/:idProject", ()=> {

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdProject = idProject
            chai.request(server)
                .delete("/api/project/delete/" + taskIdProject)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                });
        })

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdProject = "XXX"
            chai.request(server)
                .delete("/api/project/delete/" + taskIdProject)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })


        it("It should NOT DELETE an existing task (Not Login)", (done)=>{
            const taskIdProject = idProject
            chai.request(server)
                .delete("/api/project/delete/" + taskIdProject)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

});