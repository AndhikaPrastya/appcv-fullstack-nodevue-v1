const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const {response} = require("express");
let expect = chai.expect;

// Assertion style
chai.should();

chai.use(chaiHttp);

let token = "";
let idPerson = "";
let idRole = "";

describe('Tasks API Login User (Role)', () =>{
    
    /**
     * Test the Login User
     */

    it("User should be in database", (done)=>{

            chai.request(server)
                .post("/api/login")
                .send({
                    'email': 'test@gmail.com',
                    'password': '12345'
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('values').a('object');
                    token = res.body.token;
                    idPerson = res.body.values.idPerson;
                    // console.log(idPerson);
                    done();
                });
        })
});


describe('Tasks API Role', () =>{
    
    /**
     * Test the POST reoute
     */

    describe("POST /api/role/add", ()=> {

        it("It should POST a new task (Login)", (done)=>{
            const task = {
                nameRole: "Task nameRole",
                descRole: "Task descRole",
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/role/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                    idRole = res.body.values.idRole;
                done();
                });
        })

        it("It should NOT POST in new task because mandatory value has not been entered (Login)", (done)=>{
            const task = {
                nameRole: null,
                descRole: null,
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/role/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })

        it("It should NOT POST a new task (Not Login)", (done)=>{
            const task = {
                nameRole: "Task nameRole",
                descRole: "Task descRole",
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/role/add")
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })
    
    /**
     * Test the GET reoute
     */

    describe("GET /api/role", ()=> {

        it("It should GET all Role (Login)", (done)=>{
            chai.request(server)
                .get("/api/role/")
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('messages').eq('SUCCESS');
                    res.body.should.have.property('values').a('array');
                    done();
                });
        })
        

        it("It should NOT GET all Role (Not Login)", (done)=>{
            chai.request(server)
                .get("/api/role/")
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the GET by idPerson reoute
     */

    describe("GET /api/Role/idPerson", ()=> {

        it("It should GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/role/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = "XXXX";
            chai.request(server)
                .get("/api/role/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idPerson (Not Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/role/" + taskIdPerson)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })
    })

    /**
     * Test the GET by idRole reoute
     */

    describe("GET /api/role/idRole", ()=> {

        it("It should GET a task by idRole (Login)", (done)=>{
            const taskIdRole = idRole
            chai.request(server)
                .get("/api/role/idRole/" + taskIdRole)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should GET a task by idRole (Login)", (done)=>{
            const taskIdRole = "XXX"
            chai.request(server)
                .get("/api/role/idRole/" + taskIdRole)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idRole (Not Login)", (done)=>{
            const taskIdRole = idRole
            chai.request(server)
                .get("/api/role/idRole/" + taskIdRole)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the PATCH reoute
     */

    describe("PATCH /api/role/update/:idRole", ()=> {

        it("It should PATCH an existing task (Login)", (done)=>{
            const taskIdRole = idRole
            const task = {
                nameRole: "Task nameRole Update",
                descRole: "Task descRole Update",
            }
            chai.request(server)
                .patch("/api/role/update/" + taskIdRole)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                done();
                });
        })

        it("It should PATCH an existing task (Login)", (done)=>{
            const taskIdRole = idRole
            const task = {
                nameRole: null,
                descRole: null,
            }
            chai.request(server)
                .patch("/api/role/update/" + taskIdRole)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })


        it("It should NOT PATCH an existing task (Not Login)", (done)=>{
            const taskIdRole = idRole
            const task = {
                nameRole: "Task nameRole Update",
                descRole: "Task descRole Update",
            }
            chai.request(server)
                .patch("/api/role/update/" + taskIdRole)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the DELETE reoute
     */

    describe("DELETE /api/role/delete/:idRole", ()=> {

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdRole = idRole
            chai.request(server)
                .delete("/api/role/delete/" + taskIdRole)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                });
        })

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdRole = "XXX"
            chai.request(server)
                .delete("/api/role/delete/" + taskIdRole)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })


        it("It should NOT DELETE an existing task (Not Login)", (done)=>{
            const taskIdRole = idRole
            chai.request(server)
                .delete("/api/role/delete/" + taskIdRole)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

});