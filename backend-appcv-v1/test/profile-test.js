const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const {response} = require("express");
let expect = chai.expect;

// Assertion style
chai.should();

chai.use(chaiHttp);

let token = "";
let idPerson = "";
let idProfile = "";

describe('Tasks API Login User (Profile)', () =>{
    
    /**
     * Test the Login User
     */

    it("User should be in database", (done)=>{

            chai.request(server)
                .post("/api/login")
                .send({
                    'email': 'test@gmail.com',
                    'password': '12345'
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('values').a('object');
                    token = res.body.token;
                    idPerson = res.body.values.idPerson;
                    // console.log(idPerson);
                    done();
                });
        })
});


describe('Tasks API Profile', () =>{
    
    /**
     * Test the POST reoute
     */

    describe("POST /api/profile/add", ()=> {

        it("It should POST a new task (Login)", (done)=>{
            const task = {
                descProfile: "task post profile xxx",
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/profile/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                    idProfile = res.body.values.idProfile;
                done();
                });
        })

        it("It should NOT POST in new task because mandatory value has not been entered (Login)", (done)=>{
            const task = {
                descProfile: null,
                idPerson: idPerson
            }
            chai.request(server)
                .post("/api/profile/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })

        it("It should NOT POST a new task (Not Login)", (done)=>{
            const task = {
                descProfile: "task post profile",
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/profile/add")
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })
    
    /**
     * Test the GET reoute
     */

    describe("GET /api/profile", ()=> {

        it("It should GET all Profile (Login)", (done)=>{
            chai.request(server)
                .get("/api/profile/")
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('messages').eq('SUCCESS');
                    res.body.should.have.property('values').a('array');
                    done();
                });
        })

        it("It should NOT GET all profile (Not Login)", (done)=>{
            chai.request(server)
                .get("/api/profile/")
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the GET by idPerson reoute
     */

    describe("GET /api/profile/idPerson", ()=> {

        it("It should GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/profile/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

         it("It should NOT GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = "XXX";
            chai.request(server)
                .get("/api/profile/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idPerson (Not Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/profile/" + taskIdPerson)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })
    })

    /**
     * Test the GET by idProfile reoute
     */

    describe("GET /api/profile/idProfile", ()=> {

        it("It should GET a task by idProfile (Login)", (done)=>{
            const taskIdProfile = idProfile
            chai.request(server)
                .get("/api/profile/idProfile/" + taskIdProfile)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should GET a task by idProfile (Login)", (done)=>{
            const taskIdProfile = "XXXX"
            chai.request(server)
                .get("/api/profile/idProfile/" + taskIdProfile)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idProfile (Not Login)", (done)=>{
            const taskIdProfile = idProfile
            chai.request(server)
                .get("/api/profile/idProfile/" + taskIdProfile)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the PATCH reoute
     */

    describe("PATCH /api/profile/update/:idProfile", ()=> {

        it("It should PATCH an existing task (Login)", (done)=>{
            const taskIdProfile = idProfile
            const task = {
                descProfile: "task changed",
            }
            chai.request(server)
                .patch("/api/profile/update/" + taskIdProfile)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                done();
                });
        })

        it("It should NOT UPDATE in new task because mandatory value has not been entered (Login)", (done)=>{
            const taskProfile = idProfile
            const task = {
                descProfile: null,
            }
            chai.request(server)
                .patch("/api/profile/update/" + taskProfile)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })

        it("It should PATCH an existing task (Login)", (done)=>{
            const taskIdProfile = "XXXX"
            const task = {
                descProfile: "task changed",
            }
            chai.request(server)
                .patch("/api/profile/update/" + taskIdProfile)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                });
        })


        it("It should NOT PATCH an existing task (Not Login)", (done)=>{
            const taskIdProfile = idProfile
            const task = {
                descProfile: "task changed",
            }
            chai.request(server)
                .patch("/api/profile/update/" + taskIdProfile)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the DELETE reoute
     */

    describe("DELETE /api/profile/delete/:idProfile", ()=> {

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdProfile = idProfile
            chai.request(server)
                .delete("/api/profile/delete/" + taskIdProfile)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                });
        })

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdProfile = "XXXX"
            chai.request(server)
                .delete("/api/profile/delete/" + taskIdProfile)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })



        it("It should NOT DELETE an existing task (Not Login)", (done)=>{
            const taskIdProfile = idProfile
            chai.request(server)
                .delete("/api/profile/delete/" + taskIdProfile)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

});