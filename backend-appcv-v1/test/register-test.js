const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const {response} = require("express");
let expect = chai.expect;

// Assertion style
chai.should();

chai.use(chaiHttp);

let idPerson = "";
let email = "";
let password_req = "";
let token = "";

describe("POST /api/person/register", ()=> {

        it("It should POST a new task", (done)=>{
            const task = {
                namePerson: "Test 2",
                birthPlace: "test 2 Place",
                birthDate: "2020-12-12",
                gendre: "Male",
                health: "Good",
                religion: "Islam",
                email: "test2@gmail.com",
                password: "12345",
            }
            password_req = task.password;
            chai.request(server)
                .post("/api/register")
                .send(task)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('messages').eq('Person values added successfully');
                    res.body.should.be.a('object').property('values');
                    email = res.body.values.email;
                done();
                });
        })

        it("It should NOT POST in new task because mandatory value has not been entered (Login)", (done)=>{
            const task = {
                namePerson: null,
                birthPlace: null,
                birthDate:null,
                gendre: "Male",
                health: "Good",
                religion: "Islam",
                email: "test2@gmail.com",
                password: "12345",
            }
            chai.request(server)
                .post("/api/register")
                .send(task)
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })

        it("It should NOT POST a new task because email already exists", (done)=>{
            const task = {
                namePerson: "Test 2",
                birthPlace: "test 2 Place",
                birthDate: "2020-12-12",
                gendre: "Male",
                health: "Good",
                religion: "Islam",
                email: "test2@gmail.com",
                password: "12345",
            }
            // console.log(task)
            chai.request(server)
                .post("/api/register")
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.should.have.property('status').eq('ERROR');
                    res.body.should.have.property('messages').eq('Email Already Exists');
                done();
                });
        })

        
    it("User should be in database", (done)=>{

            chai.request(server)
                .post("/api/login")
                .send({
                    'email': email,
                    'password': password_req,
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('values').a('object');
                    token = res.body.token;
                    idPerson = res.body.values.idPerson;
                    done();
                });
        })

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdPerson = idPerson
            chai.request(server)
                .delete("/api/person/delete/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                });
        })

    })
