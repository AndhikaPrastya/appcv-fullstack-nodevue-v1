const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const {response} = require("express");
let expect = chai.expect;

// Assertion style
chai.should();

chai.use(chaiHttp);

let token = "";
let idPerson = "";
let idCourse = "";

describe('Tasks API Login User (Course)', () =>{
    
    /**
     * Test the Login User
     */

    it("User should be in database", (done)=>{

            chai.request(server)
                .post("/api/login")
                .send({
                    'email': 'test@gmail.com',
                    'password': '12345'
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('values').a('object');
                    token = res.body.token;
                    idPerson = res.body.values.idPerson;
                    // console.log(idPerson);
                    done();
                });
        })
});


describe('Tasks API Course', () =>{
    
    /**
     * Test the POST reoute
     */

    describe("POST /api/course/add", ()=> {

        it("It should POST a new task (Login)", (done)=>{
            const task = {
                titleCourse: "Task Title Course",
                providerCourse: "Task Provider",
                placeCourse: "Task Place",
                dateCourse: "2016-01-01",
                durationCourse: "2",
                certificateCourse: 'No',
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/course/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                    idCourse = res.body.values.idCourse;
                done();
                });
        })

        it("It should NOT POST in new task because mandatory value has not been entered (Login)", (done)=>{
            const task = {
                titleCourse: null,
                providerCourse: null,
                placeCourse: null,
                dateCourse: null,
                durationCourse: null,
                certificateCourse: null,
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/course/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })

        it("It should NOT POST a new task (Not Login)", (done)=>{
            const task = {
                titleCourse: "Task Title Course",
                providerCourse: "Task Provider",
                placeCourse: "Task Place",
                dateCourse: "2016-01-01",
                durationCourse: "2",
                certificateCourse: 'No',
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/course/add")
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })
    
    
    /**
     * Test the GET reoute
     */

    describe("GET /api/course", ()=> {

        it("It should GET all Course (Login)", (done)=>{
            chai.request(server)
                .get("/api/course/")
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('messages').eq('SUCCESS');
                    res.body.should.have.property('values').a('array');
                    done();
                });
        })

        it("It should NOT GET all Course (Not Login)", (done)=>{
            chai.request(server)
                .get("/api/course/")
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the GET by idPerson reoute
     */

    describe("GET /api/course/:idPerson", ()=> {

        it("It should GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/course/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should NOT GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = "XXX";
            chai.request(server)
                .get("/api/course/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idPerson (Not Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/course/" + taskIdPerson)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })
    })
    
    
    /**
     * Test the GET by idCourse reoute
     */

    describe("GET /api/course/idCourse", ()=> {

        it("It should GET a task by idCourse (Login)", (done)=>{
            const taskIdCourse = idCourse
            chai.request(server)
                .get("/api/course/idCourse/" + taskIdCourse)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should NOT GET a task by idCourse (Login)", (done)=>{
            const taskIdCourse = "XXX"
            chai.request(server)
                .get("/api/course/idCourse/" + taskIdCourse)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idCourse (Not Login)", (done)=>{
            const taskIdCourse = idCourse
            chai.request(server)
                .get("/api/course/idCourse/" + taskIdCourse)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the PATCH reoute
     */

    describe("PATCH /api/course/update/:idCourse", ()=> {

        it("It should PATCH an existing task (Login)", (done)=>{
            const taskIdCourse = idCourse
            const task = {
                titleCourse: "Task Title Course Update",
                providerCourse: "Task Provider Update",
                placeCourse: "Task Place Update",
                dateCourse: "2016-01-02",
                durationCourse: "4",
                certificateCourse: 'Yes',
            }
            chai.request(server)
                .patch("/api/course/update/" + taskIdCourse)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                done();
                });
        })

        it("It should NOT PATCH in new task because mandatory value has not been entered (Login)", (done)=>{
            const taskIdCourse = idCourse
            const task = {
                titleCourse: null,
                providerCourse: null,
                placeCourse: null,
                dateCourse: null,
                durationCourse: null,
                certificateCourse: null,
                idPerson: idPerson,
            }
            chai.request(server)
                .patch("/api/course/update/" + taskIdCourse)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })


        it("It should NOT PATCH an existing task (Not Login)", (done)=>{
            const taskIdCourse = idCourse
            const task = {
                titleCourse: "Task Title Course Update",
                providerCourse: "Task Provider Update",
                placeCourse: "Task Place Update",
                dateCourse: "2016-01-02",
                durationCourse: "4",
                certificateCourse: 'Yes',
            }
            chai.request(server)
                .patch("/api/course/update/" + taskIdCourse)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the DELETE reoute
     */

    describe("DELETE /api/course/delete/:idCourse", ()=> {

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdCourse = idCourse
            chai.request(server)
                .delete("/api/course/delete/" + taskIdCourse)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                });
        })

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdCourse = "XXX"
            chai.request(server)
                .delete("/api/course/delete/" + taskIdCourse)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })


        it("It should NOT DELETE an existing task (Not Login)", (done)=>{
            const taskIdCourse = idCourse
            chai.request(server)
                .delete("/api/course/delete/" + taskIdCourse)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

});