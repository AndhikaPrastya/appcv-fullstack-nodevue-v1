const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const {response} = require("express");
let expect = chai.expect;

// Assertion style
chai.should();

chai.use(chaiHttp);

let token = "";

describe('Tasks API Login User', () =>{
    
    /**
     * Test the Login User
     */

    it("User should be in database", (done)=>{

            chai.request(server)
                .post("/api/login")
                .send({
                    'email': 'test@gmail.com',
                    'password': '12345'
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('values').a('object');
                    token = res.body.token;
                    done();
                });
    })

    it("User not in database", (done)=>{

            chai.request(server)
                .post("/api/login")
                .send({
                    'email': 'wrong_email@gmail.com',
                    'password': 'wrong_Password_12345'
                })
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.should.have.property('status').eq('ERROR');
                    res.body.should.have.property('messages').eq('Incorrect email or password');
                    done();
                });
    })

    it("User incorrect password", (done)=>{

            chai.request(server)
                .post("/api/login")
                .send({
                    'email': 'test@gmail.com',
                    'password': 'wrong_Password_12345'
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.have.property('status').eq('ERROR');
                    res.body.should.have.property('messages').eq('Incorrect password');
                    done();
                });
    })



});


