const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const {response} = require("express");
let expect = chai.expect;

// Assertion style
chai.should();

chai.use(chaiHttp);

let token = "";
let idPerson = "";
let idLanguageSkill = "";

describe('Tasks API Login User (Language Skill)', () =>{
    
    /**
     * Test the Login User
     */

    it("User should be in database", (done)=>{

            chai.request(server)
                .post("/api/login")
                .send({
                    'email': 'test@gmail.com',
                    'password': '12345'
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('values').a('object');
                    token = res.body.token;
                    idPerson = res.body.values.idPerson;
                    // console.log(idPerson);
                    done();
                });
        })
});


describe('Tasks API Language Skill', () =>{
    
    /**
     * Test the POST reoute
     */

    describe("POST /api/language-skill/add", ()=> {

        it("It should POST a new task (Login)", (done)=>{
            const task = {
                language: "English (UK)",
                level: "Medium",
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/language-skill/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                    idLanguageSkill = res.body.values.idLanguageSkill;
                done();
                });
        })

        it("It should NOT POST in new task because mandatory value has not been entered (Login)", (done)=>{
            const task = {
                language: null,
                level: null,
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/language-skill/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })

        it("It should NOT POST a new task (Not Login)", (done)=>{
            const task = {
                language: "English (UK)",
                level: "Medium",
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/language-skill/add")
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })
    
    /**
     * Test the GET reoute
     */

    describe("GET /api/language-skill", ()=> {

        it("It should GET all Language Skill (Login)", (done)=>{
            chai.request(server)
                .get("/api/language-skill/")
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('messages').eq('SUCCESS');
                    res.body.should.have.property('values').a('array');
                    done();
                });
        })

        it("It should NOT GET all Language Skill (Not Login)", (done)=>{
            chai.request(server)
                .get("/api/language-skill/")
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the GET by idPerson reoute
     */

    describe("GET /api/languge-skill/idPerson", ()=> {

        it("It should GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/language-skill/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        
        it("It should NOT GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = "XXX";
            chai.request(server)
                .get("/api/language-skill/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idPerson (Not Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/language-skill/" + taskIdPerson)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })
    })

    /**
     * Test the GET by idLanguageSkill reoute
     */

    describe("GET /api/language-skill/idLanguageSkill", ()=> {

        it("It should GET a task by idLanguageSkill (Login)", (done)=>{
            const taskIdLanguageSkill = idLanguageSkill
            chai.request(server)
                .get("/api/language-skill/idLanguageSkill/" + taskIdLanguageSkill)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should GET a task by idLanguageSkill (Login)", (done)=>{
            const taskIdLanguageSkill = "XXX"
            chai.request(server)
                .get("/api/language-skill/idLanguageSkill/" + taskIdLanguageSkill)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        

        it("It should NOT GET a task by idLanguageSkill (Not Login)", (done)=>{
            const taskIdLanguageSkill = idLanguageSkill
            chai.request(server)
                .get("/api/language-skill/idLanguageSkill/" + taskIdLanguageSkill)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the PATCH reoute
     */

    describe("PATCH /api/language-skill/update/:idLanguageSkill", ()=> {

        it("It should PATCH an existing task (Login)", (done)=>{
            const taskIdLanguageSkill = idLanguageSkill
            const task = {
                language: "English (US)",
                level: "Good",
            }
            chai.request(server)
                .patch("/api/language-skill/update/" + taskIdLanguageSkill)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                done();
                });
        })

        it("It should PATCH an existing task (Login)", (done)=>{
            const taskIdLanguageSkill = "XXXX"
            const task = {
                language: "English (US)",
                level: "Good",
            }
            chai.request(server)
                .patch("/api/language-skill/update/" + taskIdLanguageSkill)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                done();
                });
        })

        it("It should PATCH an existing task (Login)", (done)=>{
            const taskIdLanguageSkill = idLanguageSkill
            const task = {
                language: null,
                level: null,
            }
            chai.request(server)
                .patch("/api/language-skill/update/" + taskIdLanguageSkill)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })


        it("It should NOT PATCH an existing task (Not Login)", (done)=>{
            const taskIdLanguageSkill = idLanguageSkill
            const task = {
                language: "English (US)",
                level: "Good",
            }
            chai.request(server)
                .patch("/api/language-skill/update/" + taskIdLanguageSkill)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the DELETE reoute
     */

    describe("DELETE /api/language-skill/delete/:idLanguageSkill", ()=> {

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdLanguageSkill = idLanguageSkill
            chai.request(server)
                .delete("/api/language-skill/delete/" + taskIdLanguageSkill)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                });
        })

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdLanguageSkill = "XXXX"
            chai.request(server)
                .delete("/api/language-skill/delete/" + taskIdLanguageSkill)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })


        it("It should NOT DELETE an existing task (Not Login)", (done)=>{
            const taskIdLanguageSkill = idLanguageSkill
            chai.request(server)
                .delete("/api/language-skill/delete/" + taskIdLanguageSkill)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

});