const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const {response} = require("express");
let expect = chai.expect;

// Assertion style
chai.should();

chai.use(chaiHttp);

let token = "";
let idPerson = "";
let idCompany = "";

describe('Tasks API Login User (Company)', () =>{
    
    /**
     * Test the Login User
     */

    it("User should be in database", (done)=>{

            chai.request(server)
                .post("/api/login")
                .send({
                    'email': 'test@gmail.com',
                    'password': '12345'
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('values').a('object');
                    token = res.body.token;
                    idPerson = res.body.values.idPerson;
                    done();
                });
        })
});


describe('Tasks API Company', () =>{
    
    /**
     * Test the POST reoute
     */

    describe("POST /api/company/add", ()=> {

        it("It should POST a new task (Login)", (done)=>{
            const task = {
                nameCompany: "Task nameCompany",
                statusEmployee: "Task statusEmployee",
                addressCompany: "Task addressCompany",
                contactCompany: "12345",
                priodFrom: "2021-01-01",
                priodTo: "2021-02-01",
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/company/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                    idCompany = res.body.values.idCompany;
                    console.log('dasdsadasdadasdsadsada',idCompany)
                done();
                });
        })

        it("It should NOT POST in new task because mandatory value has not been entered (Login)", (done)=>{
            const task = {
                nameCompany: null,
                statusEmployee: null,
                addressCompany: null,
                contactCompany: null,
                priodFrom: null,
                priodTo: null,
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/company/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })

        it("It should NOT POST a new task (Not Login)", (done)=>{
            const task = {
                nameCompany: "Task nameCompany",
                statusEmployee: "Task statusEmployee",
                addressCompany: "Task addressCompany",
                contactCompany: "12345",
                priodFrom: "2021-01-01",
                priodTo: "2021-02-01",
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/company/add")
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })
    
    
    /**
     * Test the GET reoute
     */

    describe("GET /api/company", ()=> {

        it("It should GET all company (Login)", (done)=>{
            chai.request(server)
                .get("/api/company/")
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('messages').eq('SUCCESS');
                    res.body.should.have.property('values').a('array');
                    done();
                });
        })

        it("It should NOT GET all company (Not Login)", (done)=>{
            chai.request(server)
                .get("/api/company/")
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the GET by idPerson reoute
     */

    describe("GET /api/company/:idPerson", ()=> {

        it("It should GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/company/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should NOT GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = "XXXX";
            chai.request(server)
                .get("/api/company/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idPerson (Not Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/company/" + taskIdPerson)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })
    })
    
    
    /**
     * Test the GET by idCompany reoute
     */

    describe("GET /api/company/:idCompany", ()=> {

       it("It should GET a task by idCompany (Login)", (done)=>{
            const taskIdCompany = idCompany
            chai.request(server)
                .get("/api/company/idCompany/" + taskIdCompany)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should NOT GET a task by idCompany (Login)", (done)=>{
            const taskIdCompany = "XXXX"
            chai.request(server)
                .get("/api/company/idCompany/" + taskIdCompany)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idCompany (Not Login)", (done)=>{
            const taskIdCompany = idCompany
            chai.request(server)
                .get("/api/company/idCompany/" + taskIdCompany)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the GET nested by idPerson reoute
     */

    describe("GET /api/company/nested/:idPerson", ()=> {

        it("It should GET nested a task by idPerson (Login)", (done)=>{
            const taskIdPerson = idPerson
            chai.request(server)
                .get("/api/company/nested/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should NOT GET nested a task by idPerson (Login)", (done)=>{
            const taskIdPerson = "XXX"
            chai.request(server)
                .get("/api/company/nested/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET nested a task by idPerson (Not Login)", (done)=>{
            const taskIdPerson = idPerson
            chai.request(server)
                .get("/api/company/nested/" + taskIdPerson)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the PATCH reoute
     */

    describe("PATCH /api/company/update/:idcompany", ()=> {

        it("It should PATCH an existing task (Login)", (done)=>{
            const taskIdCompany = idCompany
            const task = {
                nameCompany: "Task nameCompany Update",
                statusEmployee: "Task statusEmployee Update",
                addressCompany: "Task addressCompany Update",
                contactCompany: "123456789",
                priodFrom: "2021-01-02",
                priodTo: "2021-02-02",
            }
            chai.request(server)
                .patch("/api/company/update/" + taskIdCompany)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                done();
                });
        })

        it("It should NOT PATCH an existing task (Login)", (done)=>{
            const taskIdCompany = idCompany
            const task = {
                nameCompany: null,
                statusEmployee: null,
                addressCompany: null,
                contactCompany: null,
                priodFrom: null,
                priodTo: null,
            }
            chai.request(server)
                .patch("/api/company/update/" + taskIdCompany)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })


        it("It should NOT PATCH an existing task (Not Login)", (done)=>{
            const taskIdCompany = idCompany
            const task = {
                nameCompany: "Task nameCompany Update",
                statusEmployee: "Task statusEmployee Update",
                addressCompany: "Task addressCompany Update",
                contactCompany: "123456789",
                priodFrom: "2021-01-02",
                priodTo: "2021-02-02",
            }
            chai.request(server)
                .patch("/api/company/update/" + taskIdCompany)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the DELETE reoute
     */

    describe("DELETE /api/company/delete/:idcompany", ()=> {

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdCompany = idCompany
            chai.request(server)
                .delete("/api/company/delete/" + taskIdCompany)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                });
        })

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdCompany = "XXXX"
            chai.request(server)
                .delete("/api/company/delete/" + taskIdCompany)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })


        it("It should NOT DELETE an existing task (Not Login)", (done)=>{
            const taskIdCompany = idCompany
            chai.request(server)
                .delete("/api/company/delete/" + taskIdCompany)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

});