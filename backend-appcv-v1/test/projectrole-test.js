const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const {response} = require("express");
let expect = chai.expect;

// Assertion style
chai.should();

chai.use(chaiHttp);

let token = "";
let idProject = "";
let idProjectRole = "";

describe('Tasks API Login User (Project)', () =>{
    
    /**
     * Test the Login User
     */

    it("User should be in database", (done)=>{

            chai.request(server)
                .post("/api/login")
                .send({
                    'email': 'test@gmail.com',
                    'password': '12345'
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('values').a('object');
                    token = res.body.token;
                    idPerson = res.body.values.idPerson;
                    // console.log(idPerson);
                    done();
                });
        })
});

describe('Tasks API Project Role', () =>{
    
    /**
     * Test the POST reoute
     */

    describe("POST /api/project-role/add", ()=> {

        it("It should POST a new task (Login)", (done)=>{
            const task = {
                idProject: "PRJ-04a9efaa-4a58-456d-bb32-0d5b87a27f89",
                idRole: "ROL-1c022e61-dfda-4a24-ab33-b442c25dcdb6"
            }
            chai.request(server)
                .post("/api/project-role/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                    idProjectRole = res.body.values.idProjectRole;
                done();
                });
        })

        it("It should NOT POST in new task because mandatory value has not been entered (Login)", (done)=>{
            const task = {
                idProject: null,
                idRole: null
            }
            chai.request(server)
                .post("/api/project-role/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })

        it("It should NOT POST a new task (Not Login)", (done)=>{
            const task = {
                idProject: "PRJ-04a9efaa-4a58-456d-bb32-0d5b87a27f89",
                idRole: "ROL-1c022e61-dfda-4a24-ab33-b442c25dcdb6"
            }
            chai.request(server)
                .post("/api/project-role/add")
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the GET reoute
     */

    describe("GET /api/project-role", ()=> {

        it("It should GET all Project role (Login)", (done)=>{
            chai.request(server)
                .get("/api/project-role/")
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('messages').eq('SUCCESS');
                    res.body.should.have.property('values').a('array');
                    done();
                });
        })

        it("It should NOT GET all Project (Not Login)", (done)=>{
            chai.request(server)
                .get("/api/project-role/")
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the GET by idProject reoute
     */

    describe("GET /api/project-role/:idProject", ()=> {

        it("It should GET a task by idProject (Login)", (done)=>{
            const taskIdProject = idProject
            chai.request(server)
                .get("/api/project-role/" + taskIdProject)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should NOT GET a task by idProject (Login)", (done)=>{
            const taskIdProject = "XXX"
            chai.request(server)
                .get("/api/project-role/" + taskIdProject)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idProject (Not Login)", (done)=>{
            const taskIdProject = idProject
            chai.request(server)
                .get("/api/project-role/" + taskIdProject)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

     /**
     * Test the PATCH reoute
     */

    describe("PATCH /api/project-role/update/:idProjectRole", ()=> {

        it("It should PATCH an existing task (Login)", (done)=>{
            const taskIdProjectRole = idProjectRole
            const task = {
                idProject: "PRJ-04a9efaa-4a58-456d-bb32-0d5b87a27f89",
                idRole: "ROL-1c022e61-dfda-4a24-ab33-b442c25dcdb6"
            }
            chai.request(server)
                .patch("/api/project-role/update/" + taskIdProjectRole)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                done();
                });
        })

        it("It should NOT PATCH in new task because mandatory value has not been entered (Login)", (done)=>{
            const taskIdProjectRole = idProjectRole
            const task = {
                idProject: null,
                idRole: null
            }
            chai.request(server)
                .patch("/api/project-role/update/" + taskIdProjectRole)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })

        it("It should NOT PATCH an existing task (Not Login)", (done)=>{
            const taskIdProjectRole = idProjectRole
            const task = {
                idProject: "PRJ-04a9efaa-4a58-456d-bb32-0d5b87a27f89",
                idRole: "ROL-1c022e61-dfda-4a24-ab33-b442c25dcdb6"
            }
            chai.request(server)
                .patch("/api/project-role/update/" + taskIdProjectRole)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the DELETE reoute
     */

    describe("DELETE /api/project-role/delete/:idProjectRole", ()=> {

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdProjectRole = idProjectRole
            chai.request(server)
                .delete("/api/project-role/delete/" + taskIdProjectRole)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                });
        })

        it("It should NOT DELETE an existing task (Login)", (done)=>{
            const taskIdProjectRole = "XXX"
            chai.request(server)
                .delete("/api/project-role/delete/" + taskIdProjectRole)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT DELETE an existing task (Not Login)", (done)=>{
            const taskIdProjectRole = idProjectRole
            chai.request(server)
                .delete("/api/project-role/delete/" + taskIdProjectRole)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

});