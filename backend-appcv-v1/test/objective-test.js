const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const {response} = require("express");
const expect = chai.expect;


// Assertion style
chai.should();

chai.use(chaiHttp);

let token = "";
let idPerson = "";
let idObjective = "";

describe('Tasks API Login User (Objective)', () =>{
    
    /**
     * Test the Login User
     */

    it("User should be in database", (done)=>{

            chai.request(server)
                .post("/api/login")
                .send({
                    'email': 'test@gmail.com',
                    'password': '12345'
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('values').a('object');
                    token = res.body.token;
                    idPerson = res.body.values.idPerson;
                    // console.log(idPerson);
                    done();
                });
        })
});


describe('Tasks API Objective', () =>{
    
    /**
     * Test the POST reoute
     */

    describe("POST /api/objective/add", ()=> {

        it("It should POST a new task (Login)", (done)=>{
            const task = {
                descObjective: "task post objective 2222",
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/objective/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                    idObjective = res.body.values.idObjective;
                done();
                });
        })

        it("It should NOT POST in new task because mandatory value has not been entered (Login)", (done)=>{
            const task = {
                descObjective: null,
                idPerson: idPerson
            }
            chai.request(server)
                .post("/api/objective/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })

        it("It should NOT POST a new task (Not Login)", (done)=>{
            const task = {
                descObjective: "task post objective",
                idPerson: idPerson,
            }
            chai.request(server)
                .post("api/objective/add")
                .send(task)
                .end((err, res) => {
                    res.should.have.status(403);
                done();
                });
        })

    })
    
    /**
     * Test the GET reoute
     */

    describe("GET /api/objective", ()=> {

        it("It should GET all Objective (Login)", (done)=>{
            chai.request(server)
                .get("/api/objective/")
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('messages').eq('SUCCESS');
                    res.body.should.have.property('values').a('array');
                    done();
                });
        })
        

        it("It should NOT GET all Objective (Not Login)", (done)=>{
            chai.request(server)
                .get("/api/objective/")
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the GET by idPerson reoute
     */

    describe("GET /api/objective/idPerson", ()=> {

        it("It should GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/objective/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should NOT GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = "XXX";
            chai.request(server)
                .get("/api/objective/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idPerson (Not Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/objective/" + taskIdPerson)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })
    })

    /**
     * Test the GET by idObjective reoute
     */

    describe("GET /api/objective/idObjective", ()=> {

        it("It should GET a task by IdObjective (Login)", (done)=>{
            const taskIdObjective = idObjective
            chai.request(server)
                .get("/api/objective/idObjective/" + taskIdObjective)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should GET a task by IdObjective (Login)", (done)=>{
            const taskIdObjective = "XXXX"
            chai.request(server)
                .get("/api/objective/idObjective/" + taskIdObjective)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by IdObjective (Not Login)", (done)=>{
            const taskIdObjective = idObjective
            chai.request(server)
                .get("/api/objective/idObjective/" + taskIdObjective)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the PATCH reoute
     */

    describe("PATCH /api/objective/update/:idObjective", ()=> {

        it("It should PATCH an existing task (Login)", (done)=>{
            const taskIdObjective = idObjective
            const task = {
                descObjective: "task changed",
            }
            chai.request(server)
                .patch("/api/objective/update/" + taskIdObjective)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                done();
                });
        })

        it("It should NOT UPDATE in new task because mandatory value has not been entered (Login)", (done)=>{
            const taskIdObjective = idObjective
            const task = {
                descObjective: null,
            }
            chai.request(server)
                .patch("/api/objective/update/" + taskIdObjective)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })

        it("It should PATCH an existing task (Login)", (done)=>{
            const taskIdObjective = "XXXX"
            const task = {
                descObjective: "task changed",
            }
            chai.request(server)
                .patch("/api/objective/update/" + taskIdObjective)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                });
        })


        it("It should NOT PATCH an existing task (Not Login)", (done)=>{
            const taskIdObjective = idObjective
            const task = {
                descObjective: "task changed",
            }
            chai.request(server)
                .patch("/api/objective/update/" + taskIdObjective)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the DELETE reoute
     */

    describe("DELETE /api/objective/delete/:idObjective", ()=> {

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdObjective = idObjective
            chai.request(server)
                .delete("/api/objective/delete/" + taskIdObjective)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                });
        })

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdObjective = "XXXX"
            chai.request(server)
                .delete("/api/objective/delete/" + taskIdObjective)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })


        it("It should NOT DELETE an existing task (Not Login)", (done)=>{
            const taskIdObjective = idObjective
            chai.request(server)
                .delete("/api/objective/delete/" + taskIdObjective)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

});

