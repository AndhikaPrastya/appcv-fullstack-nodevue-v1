const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const {response} = require("express");
let expect = chai.expect;


// Assertion style
chai.should();

chai.use(chaiHttp);

let token = "";
let idPerson = "";

describe('Tasks API Login User (Person)', () =>{
    
    /**
     * Test the Registrasi and Login User
     */

it("It should POST a new task", (done)=>{
            const task = {
                namePerson: "Test 3",
                birthPlace: "test 3 Place",
                birthDate: "2020-12-12",
                gendre: "Male",
                health: "Good",
                religion: "Islam",
                email: "test3@gmail.com",
                password: "12345",
            }
            password_req = task.password;
            // console.log(password_req)
            chai.request(server)
                .post("/api/register")
                .send(task)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('messages').eq('Person values added successfully');
                    res.body.should.be.a('object').property('values');
                    email = res.body.values.email;
                done();
                });
        })
        
        it("User should be in database", (done)=>{

                chai.request(server)
                    .post("/api/login")
                    .send({
                        'email': email,
                        'password': password_req,
                    })
                    .end((err, res) => {
                        res.should.have.status(201);
                        res.body.should.be.a('object');
                        res.body.should.have.property('token');
                        res.body.should.have.property('status').eq('OK');
                        res.body.should.have.property('values').a('object');
                        token = res.body.token;
                        idPerson = res.body.values.idPerson;
                        done();
                    });
        })
});


describe('Tasks API Person', () =>{
    
    
    /**
     * Test the GET reoute
     */

    describe("GET /api/person", ()=> {

        it("It should GET all person (Login)", (done)=>{
            chai.request(server)
                .get("/api/person/")
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('messages').eq('SUCCESS');
                    res.body.should.have.property('values').a('array');
                    done();
                });
        })

        it("It should NOT GET all Person (Not Login)", (done)=>{
            chai.request(server)
                .get("/api/person/")
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the GET by idPerson reoute
     */

    describe("GET /api/person/idPerson", ()=> {

        it("It should GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/person/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        
        it("It should NOT GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = "XXX";
            chai.request(server)
                .get("/api/person/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idPerson (Not Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/person/" + taskIdPerson)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })
    })

    /**
     * Test the GET Photo reoute
     */

    describe("GET /api/person/photo", ()=> {

        it("It should GET a task by phptp (Login)", (done)=>{
            const taskPhoto = "profile-1619412718818.png";
            chai.request(server)
                .get("/api/person/photo/" + taskPhoto)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                });
        })

        it("It should NOT GET a task by photo (Not Login)", (done)=>{
            const taskPhoto = "profile-1619412718818.png";
            chai.request(server)
                .get("/api/person/photo/" + taskPhoto)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })
    })


    /**
     * Test the PATCH reoute
     */

    describe("PATCH /api/person/update/:idPerson", ()=> {

        it("It should PATCH an existing task (Login)", (done)=>{
            const taskIdPerson = idPerson
            const task = {
                namePerson : "task namePerson Update",
                birthPlace: "task birthPlace Update",
                birthDate: "2020-01-01",
                gendre: "Male",
                health: "Good",
                religion: "religion",
            }
            chai.request(server)
                .patch("/api/person/update/" + taskIdPerson)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                done();
                });
        })

        it("It should NOT PATCH an existing task (Login)", (done)=>{
            const taskIdPerson = idPerson
            const task = {
                namePerson : null,
                birthPlace: null,
                birthDate: null,
                gendre: null,
                health: null,
                religion: null,
            }
            chai.request(server)
                .patch("/api/person/update/" + taskIdPerson)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                done();
                });
        })


        it("It should NOT PATCH an existing task (Not Login)", (done)=>{
            const taskIdPerson = idPerson
            const task = {
                namePerson : "task namePerson Update",
                birthPlace: "task birthPlace Update",
                birthDate: "2020-01-01",
                gendre: "Male",
                health: "Good",
                religion: "religion",
            }
            chai.request(server)
                .patch("/api/person/update/" + taskIdPerson)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the DELETE reoute
     */

    describe("DELETE /api/objective/delete/:idObjective", ()=> {

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdPerson = idPerson
            chai.request(server)
                .delete("/api/person/delete/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                });
        })

        it("It should NOT DELETE an existing task (Login)", (done)=>{
            const taskIdPerson = "XXX"
            chai.request(server)
                .delete("/api/person/delete/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })


        it("It should NOT DELETE an existing task (Not Login)", (done)=>{
            const taskIdPerson = idPerson
            chai.request(server)
                .delete("/api/person/delete/" + taskIdPerson)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    

});
