const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const {response} = require("express");
let expect = chai.expect;

// Assertion style
chai.should();

chai.use(chaiHttp);

let token = "";
let idProjectPeriode = "";

describe('Tasks API Login User (Project Periode)', () =>{
    
    /**
     * Test the Login User
     */

    it("User should be in database", (done)=>{

            chai.request(server)
                .post("/api/login")
                .send({
                    'email': 'test@gmail.com',
                    'password': '12345'
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('values').a('object');
                    token = res.body.token;
                    idPerson = res.body.values.idPerson;
                    // console.log(idPerson);
                    done();
                });
        })
});


describe('Tasks API project Periode', () =>{
    
    /**
     * Test the POST reoute
     */

    describe("POST /api/project-periode/add", ()=> {

        it("It should POST a new task (Login)", (done)=>{
            const task = {
                idProject: "PRJ-04a9efaa-4a58-456d-bb32-0d5b87a27f89",
                priodFrom: "2021-01-02",
                priodTo: "2021-01-05",
            }
            chai.request(server)
                .post("/api/project-periode/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                    idProjectPeriode = res.body.values.idProjectPeriode;
                done();
                });
        })

        it("It should NOT POST in new task because mandatory value has not been entered (Login)", (done)=>{
            const task = {
                idProject: null,
                priodFrom: null,
                priodTo: null,
            }
            chai.request(server)
                .post("/api/project-periode/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })

        it("It should NOT POST a new task (Not Login)", (done)=>{
            const task = {
                idProject: "PRJ-04a9efaa-4a58-456d-bb32-0d5b87a27f89",
                priodFrom: "2021-01-02",
                priodTo: "2021-01-05",
            }
            chai.request(server)
                .post("/api/project-periode/add")
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })
    
    
    /**
     * Test the GET reoute
     */

    describe("GET /api/project-periode/:idPerson", ()=> {

        it("It should GET all project Periode (Login)", (done)=>{
            const taskIdPerson = idPerson
            chai.request(server)
                .get("/api/project-periode/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('messages').eq('SUCCESS');
                    res.body.should.have.property('values').a('array');
                    done();
                });
        })
        
        it("It should NOT GET all project Periode (Login)", (done)=>{
            const taskIdPerson = "XXX"
            chai.request(server)
                .get("/api/project-periode/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                    done();
                });
        })


        it("It should NOT GET all company (Not Login)", (done)=>{
            const taskIdPerson = idPerson
            chai.request(server)
                .get("/api/project-periode/" + taskIdPerson)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the GET by idProjectPeriode reoute
     */

    describe("GET /api/project-periode/idProjectPeriode/:idProjectPeriode", ()=> {

        it("It should GET a task by idProjectPeriode (Login)", (done)=>{
            const taskIdProjectPeriode = idProjectPeriode
            chai.request(server)
                .get("/api/project-periode/idProjectPeriode/" + taskIdProjectPeriode)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should NOT GET a task by idProjectPeriode (Login)", (done)=>{
            const taskIdProjectPeriode = "XXX"
            chai.request(server)
                .get("/api/project-periode/idProjectPeriode/" + taskIdProjectPeriode)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idProject (Not Login)", (done)=>{
            const taskIdProjectPeriode = idProjectPeriode
            chai.request(server)
                .get("/api/project-periode/idProjectPeriode/" + taskIdProjectPeriode)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the PATCH reoute
     */

    describe("PATCH /api/project-periode/update/:idProjectPeriode", ()=> {

        it("It should PATCH an existing task (Login)", (done)=>{
            const taskIdProjectPeriode = idProjectPeriode
            const task = {
                idProject: "PRJ-04a9efaa-4a58-456d-bb32-0d5b87a27f89",
                priodFrom: "2021-01-04",
                priodTo: "2021-01-06",
            }
            chai.request(server)
                .patch("/api/project-periode/update/" + taskIdProjectPeriode)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                done();
                });
        })

        it("It should NOT PATCH an existing task (Login)", (done)=>{
            const taskIdProjectPeriode = idProjectPeriode
            const task = {
                idProject: null,
                priodFrom: null,
                priodTo: null,
            }
            chai.request(server)
                .patch("/api/project-periode/update/" + taskIdProjectPeriode)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                done();
                });
        })

        it("It should NOT PATCH an existing task (Not Login)", (done)=>{
            const taskIdProjectPeriode = idProjectPeriode
            const task = {
                idProject: "PRJ-04a9efaa-4a58-456d-bb32-0d5b87a27f89",
                priodFrom: "2021-01-04",
                priodTo: "2021-01-06",
            }
            chai.request(server)
                .patch("/api/project-periode/update/" + taskIdProjectPeriode)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })


    /**
     * Test the DELETE reoute
     */

    describe("DELETE /api/project-periode/delete/:idProjectPeriode", ()=> {

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdProjectPeriode = idProjectPeriode
            chai.request(server)
                .delete("/api/project-periode/delete/" + taskIdProjectPeriode)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                });
        })

        it("It should NOT DELETE an existing task (Login)", (done)=>{
            const taskIdProjectPeriode = "XXX"
            chai.request(server)
                .delete("/api/project-periode/delete/" + taskIdProjectPeriode)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })


        it("It should NOT DELETE an existing task (Not Login)", (done)=>{
            const taskIdProjectPeriode = idProjectPeriode
            chai.request(server)
                .delete("/api/project-periode/delete/" + taskIdProjectPeriode)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

});