const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const {response} = require("express");
let expect = chai.expect;

// Assertion style
chai.should();

chai.use(chaiHttp);

let token = "";
let idPerson = "";
let idEducation = "";

describe('Tasks API Login User (Education)', () =>{
    
    /**
     * Test the Login User
     */

    it("User should be in database", (done)=>{

            chai.request(server)
                .post("/api/login")
                .send({
                    'email': 'test@gmail.com',
                    'password': '12345'
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('values').a('object');
                    token = res.body.token;
                    idPerson = res.body.values.idPerson;
                    // console.log(idPerson);
                    done();
                });
        })
});


describe('Tasks API Education', () =>{
    
    /**
     * Test the POST reoute
     */

    describe("POST /api/education/add", ()=> {

        it("It should POST a new task (Login)", (done)=>{
            const task = {
                school: "Universitas Muhammadiyah Cirebon",
                degree: "S1",
                subject: "Teknik Informatika",
                priodFrom: 2016-01-01,
                priodTo: 2020-01-01,
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/education/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                    idEducation = res.body.values.idEducation;
                done();
                });
        })

        it("It should NOT POST in new task because mandatory value has not been entered (Login)", (done)=>{
            const task = {
                school: null,
                degree: null,
                subject: null,
                priodFrom: null,
                priodTo: null,
                idPerson: idPerson
            }
            chai.request(server)
                .post("/api/education/add")
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.have.property('status').eq('ERROR');
                done();
                });
        })

        it("It should NOT POST a new task (Not Login)", (done)=>{
            const task = {
                school: "Universitas Muhammadiyah Cirebon",
                degree: "S1",
                subject: "Teknik Informatika",
                priodFrom: "2016-01-01",
                priodTo: "2020-01-01",
                idPerson: idPerson,
            }
            chai.request(server)
                .post("/api/education/add")
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })
    
    
    /**
     * Test the GET reoute
     */

    describe("GET /api/education", ()=> {

        it("It should GET all Education (Login)", (done)=>{
            chai.request(server)
                .get("/api/education/")
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.have.property('messages').eq('SUCCESS');
                    res.body.should.have.property('values').a('array');
                    done();
                });
        })

        it("It should NOT GET all education (Not Login)", (done)=>{
            chai.request(server)
                .get("/api/education/")
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the GET by idPerson reoute
     */

    describe("GET /api/education/idPerson", ()=> {

        it("It should GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/education/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should NOT GET a task by idPerson (Login)", (done)=>{
            const taskIdPerson = "XXXX";
            chai.request(server)
                .get("/api/education/" + taskIdPerson)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idPerson (Not Login)", (done)=>{
            const taskIdPerson = idPerson;
            chai.request(server)
                .get("/api/education/" + taskIdPerson)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })
    })
    
    
    /**
     * Test the GET by idEducation reoute
     */

    describe("GET /api/education/idEducation", ()=> {

        it("It should GET a task by idEducation (Login)", (done)=>{
            const taskIdEducation = idEducation
            chai.request(server)
                .get("/api/education/idEducation/" + taskIdEducation)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                done();
                });
        })

        it("It should NOT GET a task by idEducation (Login)", (done)=>{
            const taskIdEducation = "XXX"
            chai.request(server)
                .get("/api/education/idEducation/" + taskIdEducation)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })

        it("It should NOT GET a task by idEducation (Not Login)", (done)=>{
            const taskIdEducation = idEducation
            chai.request(server)
                .get("/api/education/idEducation/" + taskIdEducation)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the PATCH reoute
     */

    describe("PATCH /api/education/update/:idEducation", ()=> {

        it("It should PATCH an existing task (Login)", (done)=>{
            const taskIdEducation = idEducation
            const task = {
                school: "Universitas Muhammadiyah Cirebon",
                degree: "D3",
                subject: "Teknik Industri",
            }
            chai.request(server)
                .patch("/api/education/update/" + taskIdEducation)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eq('OK');
                    res.body.should.be.a('object').property('values');
                done();
                });
        })


        it("It should NOT PATCH an existing task (Login)", (done)=>{
            const taskIdEducation = idEducation
            const task = {
                school: null,
                degree: null,
                subject: null,
            }
            chai.request(server)
                .patch("/api/education/update/" + taskIdEducation)
                .send(task)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(500);
                done();
                });
        })


        it("It should NOT PATCH an existing task (Not Login)", (done)=>{
            const taskIdEducation = idEducation
            const task = {
                school: "Universitas Muhammadiyah Cirebon",
                degree: "D3",
                subject: "Teknik Industri",
            }
            chai.request(server)
                .patch("/api/education/update/" + taskIdEducation)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

    /**
     * Test the DELETE reoute
     */

    describe("DELETE /api/education/delete/:idEducation", ()=> {

        it("It should DELETE an existing task (Login)", (done)=>{
            const taskIdEducation = idEducation
            chai.request(server)
                .delete("/api/education/delete/" + taskIdEducation)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                });
        })

        it("It should NOT DELETE an existing task (Login)", (done)=>{
            const taskIdEducation = "XXXX"
            chai.request(server)
                .delete("/api/education/delete/" + taskIdEducation)
                .set({ Authorization: `Bearer ${token}`})
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                });
        })


        it("It should NOT DELETE an existing task (Not Login)", (done)=>{
            const taskIdEducation = idEducation
            chai.request(server)
                .delete("/api/education/delete/" + taskIdEducation)
                .end((err, res) => {
                    res.should.have.status(401);
                done();
                });
        })

    })

});