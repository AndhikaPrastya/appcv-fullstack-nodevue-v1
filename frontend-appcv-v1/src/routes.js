import Vue from 'vue';
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

import Home from '@/components/Home.vue'
import Login from '@/components/auth/Login.vue'
import Register from '@/components/auth/Register.vue'
import viewPerson from '@/components/person-component/viewPerson.vue'
import updatePerson from '@/components/person-component/updatePerson.vue'
import updatePhoto from '@/components/person-component/updatePhoto.vue'
import viewObjective from '@/components/objective-component/viewObjective.vue'
import addObjective from '@/components/objective-component/addObjective.vue'
import updateObjective from '@/components/objective-component/updateObjective.vue'
import viewProfile from '@/components/profile-component/viewProfile.vue'
import addProfile from '@/components/profile-component/addProfile.vue'
import updateProfile from '@/components/profile-component/updateProfile.vue'
import viewEducation from '@/components/education-component/viewEducation.vue'
import addEducation from '@/components/education-component/addEducation.vue'
import updateEducation from '@/components/education-component/updateEducation.vue'
import viewCourse from '@/components/course-component/viewCourse.vue'
import addCourse from '@/components/course-component/addCourse.vue'
import updateCourse from '@/components/course-component/updateCourse.vue'
import viewCompany from '@/components/company-component/viewCompany.vue'
import addCompany from '@/components/company-component/addCompany.vue'
import updateCompany from '@/components/company-component/updateCompany.vue'
import viewProject from '@/components/project-component/viewProject.vue'
import addProject from '@/components/project-component/addProject.vue'
import updateProject from '@/components/project-component/updateProject.vue'
import viewLanguageSkill from '@/components/languageskill-component/viewLanguageSkill.vue'
import addLanguageSkill from '@/components/languageskill-component/addLanguageSkill.vue'
import updateLanguageSkill from '@/components/languageskill-component/updateLanguageSkill.vue'
import viewRole from '@/components/role-component/viewRole.vue'
import addRole from '@/components/role-component/addRole.vue'
import updateRole from '@/components/role-component/updateRole.vue'
import viewCompanyRole from '@/components/companyrole-component/viewCompanyRole.vue'
import addCompanyRole from '@/components/companyrole-component/addCompanyRole.vue'
// import updateCompanyRole from '@/components/companyrole-component/updateCompanyRole.vue'
import viewProjectRole from '@/components/projectrole-component/viewProjectRole.vue'
import addProjectRole from '@/components/projectrole-component/addProjectRole.vue'
// import updateProjectRole from '@/components/projectrole-component/updateProjectRole.vue'
import viewProjectPeriode from '@/components/projectperiode-component/viewProjectPeriode.vue'
import addProjectPeriode from '@/components/projectperiode-component/addProjectPeriode.vue'
import updateProjectPeriode from '@/components/projectperiode-component/updateProjectPeriode.vue'

// import jwt_decode from "jwt-decode";

// const token = localStorage.getItem("token");
// const decoded = jwt_decode(token);

// const id = decoded.idPerson;

Vue.use(VueSweetalert2);

function lockRoute(to, from, next)
{
    var auth= false;
    let accessToken = localStorage.getItem('token');
    if(accessToken && accessToken !== '')
    auth = true;
    else
    auth= false;
    if(auth) 
    {
    next();
    } 
    else
    {
    next('/'); 
    }
}

const routes = [
    {
        name: '/',
        path: '/',
        component: Login
    },
    {
        name: 'register',
        path: '/register',
        component: Register
    },
    {
        name: 'home',
        path: '/home',
        beforeEnter : lockRoute,
        component: Home
    },
    {
        name: 'viewPerson',
        path: '/person/view',
        beforeEnter : lockRoute,
        component: viewPerson
    },
    {
        name: 'updatePerson',
        path: '/person/update/:id',
        beforeEnter : lockRoute,
        component: updatePerson
    },
    {
        name: 'updatePhoto',
        path: '/person/update-photo/:id',
        beforeEnter : lockRoute,
        component: updatePhoto
    },
    {
        name: 'viewObjective',
        path: '/objective/view',
        beforeEnter : lockRoute,
        component: viewObjective
    },
    {
        name: 'addObjective',
        path: '/objective/add',
        beforeEnter : lockRoute,
        component: addObjective
    },
    {
        name: 'updateObjective',
        path: '/objective/update/:id',
        beforeEnter : lockRoute,
        component: updateObjective
    },
    {
        name: 'viewProfile',
        path: '/profile/view',
        beforeEnter : lockRoute,
        component: viewProfile
    },
    {
        name: 'addProfile',
        path: '/profile/add',
        beforeEnter : lockRoute,
        component: addProfile
    },
    {
        name: 'updateProfile',
        path: '/profile/update/:id',
        beforeEnter : lockRoute,
        component: updateProfile
    },
    {
        name: 'viewEducation',
        path: '/education/view',
        beforeEnter : lockRoute,
        component: viewEducation
    },
    {
        name: 'addEducation',
        path: '/education/add',
        beforeEnter : lockRoute,
        component: addEducation
    },
    {
        name: 'updateEducation',
        path: '/education/update/:id',
        beforeEnter : lockRoute,
        component: updateEducation
    },
    {
        name: 'viewCourse',
        path: '/course/view',
        beforeEnter : lockRoute,
        component: viewCourse
    },
    {
        name: 'addCourse',
        path: '/course/add',
        beforeEnter : lockRoute,
        component: addCourse
    },
    {
        name: 'updateCourse',
        path: '/course/update/:id',
        beforeEnter : lockRoute,
        component: updateCourse
    },
    {
        name: 'viewCompany',
        path: '/company/view',
        beforeEnter : lockRoute,
        component: viewCompany
    },
    {
        name: 'addCompany',
        path: '/company/add',
        beforeEnter : lockRoute,
        component: addCompany
    },
    {
        name: 'updateCompany',
        path: '/company/update/:id',
        beforeEnter : lockRoute,
        component: updateCompany
    },
    {
        name: 'viewProject',
        path: '/project/view',
        beforeEnter : lockRoute,
        component: viewProject
    },
    {
        name: 'addProject',
        path: '/project/add',
        beforeEnter : lockRoute,
        component: addProject
    },
    {
        name: 'updateProject',
        path: '/project/update/:id',
        beforeEnter : lockRoute,
        component: updateProject
    },
    {
        name: 'viewLanguageSkill',
        path: '/languageskill/view',
        beforeEnter : lockRoute,
        component: viewLanguageSkill
    },
    {
        name: 'addLanguageSkill',
        path: '/languageskill/add',
        beforeEnter : lockRoute,
        component: addLanguageSkill
    },
    {
        name: 'updateLanguageSkill',
        path: '/languageskill/update/:id',
        beforeEnter : lockRoute,
        component: updateLanguageSkill
    },
    {
        name: 'viewRole',
        path: '/role/view',
        beforeEnter : lockRoute,
        component: viewRole
    },
    {
        name: 'addRole',
        path: '/role/add',
        beforeEnter : lockRoute,
        component: addRole
    },
    {
        name: 'updateRole',
        path: '/role/update/:id',
        beforeEnter : lockRoute,
        component: updateRole
    },
    {
        name: 'viewCompanyRole',
        path: '/companyrole/view/:id',
        beforeEnter : lockRoute,
        component: viewCompanyRole
    },
    {
        name: 'addCompanyRole',
        path: '/companyrole/add/:id',
        beforeEnter : lockRoute,
        component: addCompanyRole
    },
    // {
    //     name: 'updateCompanyRole',
    //     path: '/companyrole/update/:id',
    //     beforeEnter : lockRoute,
    //     component: updateCompanyRole
    // },
    {
        name: 'viewProjectRole',
        path: '/projectrole/view/:id',
        beforeEnter : lockRoute,
        component: viewProjectRole
    },
    {
        name: 'addProjectRole',
        path: '/projectrole/add/:id',
        beforeEnter : lockRoute,
        component: addProjectRole
    },
    // {
    //     name: 'updateProjectRole',
    //     path: '/projectrole/update/:id',
    //     beforeEnter : lockRoute,
    //     component: updateProjectRole
    // },
    {
        name: 'viewProjectPeriode',
        path: '/projectperiode/view',
        beforeEnter : lockRoute,
        component: viewProjectPeriode
    },
    {
        name: 'addProjectPeriode',
        path: '/projectperiode/add',
        beforeEnter : lockRoute,
        component: addProjectPeriode
    },
    {
        name: 'updateProjectPeriode',
        path: '/projectperiode/update/:id',
        beforeEnter : lockRoute,
        component: updateProjectPeriode
    },
    

];
 
export default routes